# PhotoEditorSDK
За основу был взят следующий [репозиторий](https://github.com/eventtus/photo-editor-android)

Бибилотека работает в двух режимах - фоторедактор и рисование подписи aka paint

# Как это чудо использовать v0.15.0

Создание фрагмент-редактор:

<pre>
PhotoEditorFragment.create(
                @NonNull  %путь для сохранения%,
                @Nullable %время для штампа%,
                          %качество%,
                @Nullable %если не null то появится возможность выбора MapMobile-sticker%,
                @Nullable %стикер по умолчанию, если null то будет отображаться "Нет стикера"%,
                @Nullable %фоторедактор имеет режимы%,
                          %если true то будет отображено поле "Добавить подпись"%,
                          %если true то будет требоваться добавить значение в поле "Добавить подпись"%,
                          %если true и date != null то на фотографию будет поставлен штамп со времене%м
        );

</pre>

# Как добавить к проекту

Последние версии можно найти на [artifactory](http://repo.geo4.pro/artifactory/android/com/gradoservice/android/photoeditor/photoeditor-view-library/)

Библиотека photoeditor-view-library имеет зависимость [photoeditorsdk](http://repo.geo4.pro/artifactory/android/com/gradoservice/android/photoeditor/photoeditor-sdk/)

## Gradle

<pre>
implementation('com.gradoservice.android.photoeditor:photoeditor-view-library:%latest_version%@aar') {
  transitive = true
}
</pre>