package com.ahmedadeltito.photoeditor

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import android.view.ViewGroup
import org.junit.Before
import org.junit.Rule
import org.junit.Test



class AddCaptionModeTest : IniterFragment() {

    @get:Rule
    var mFragmentTestRule = FragmentTestRule(getTestFragment())

    val clickerFragment = ClickerFragment(mFragmentTestRule)

    override fun isNeedAddCaptionMode(): Boolean {
        return true
    }

    @Before
    fun fragmentCanBeInstantiated() {
        mFragmentTestRule.launchActivity(null)
    }

    @Test
    fun addCaptionEditTextIsVisible() {
        onView(withId(R.id.add_caption_et)).check(matches(isDisplayed())).check(matches(withHint(R.string.gs_photoeditor_lib_add_a_caption)))
    }

    @Test
    fun addCaptionEditTextClicked() {
        //не правильно работает
        var oldBottom = 0
        val imageView = onView(withId(R.id.parent_image_rl))
        imageView.check { view, noViewFoundException ->
            run{
                val coords = intArrayOf(0, 0)
                view.getLocationOnScreen(coords)
                val absoluteTop = coords[1]
                val absoluteBottom = coords[1] + view.height
                oldBottom = view.bottom
            }
        }

        onView(withId(R.id.add_caption_et)).perform(click()).check { view, _ ->
            run {
                (view.layoutParams as ViewGroup.MarginLayoutParams).leftMargin = 0
                (view.layoutParams as ViewGroup.MarginLayoutParams).topMargin = 0
                (view.layoutParams as ViewGroup.MarginLayoutParams).rightMargin = 0
                (view.layoutParams as ViewGroup.MarginLayoutParams).bottomMargin == 0
            }
        }
        //todo и как это проверять?
        onView(withId(R.id.parent_image_rl)).check { view, noViewFoundException ->
            run {
                assert(view.bottom == oldBottom)
            }
        }


    }

}