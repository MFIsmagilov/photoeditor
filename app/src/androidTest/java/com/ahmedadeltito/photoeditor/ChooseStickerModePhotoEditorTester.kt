package com.ahmedadeltito.photoeditor

import android.app.Activity.RESULT_OK
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.RootMatchers.isDialog
import androidx.test.espresso.matcher.ViewMatchers.*
import com.ahmedadeltito.photoeditor.utils.getRandomInt
import com.ahmedadeltito.photoeditor.utils.getRandomSting
import com.gradoservice.photoeditorviewlibrary.models.Label
import com.gradoservice.photoeditorviewlibrary.photoeditor.PhotoEditorFragment
import org.hamcrest.Matchers.not
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class ChooseStickerModePhotoEditorTester : IniterFragment() {

    @get:Rule
    var mFragmentTestRule = FragmentTestRule(getTestFragment())

    val clickerFragment = ClickerFragment(mFragmentTestRule)

    @Before
    fun fragmentCanBeInstantiated() {
        mFragmentTestRule.launchActivity(null)
    }

    override fun getTestLabels(): List<Label>? {
        val size = getRandomInt()
        val list = mutableListOf<Label>()
        for (i in 1..size) {
            list.add(Label(i.toLong(), getRandomSting(), getRandomSting()))
        }
        labels = list
        return list
    }

    override fun getTestFragmentMode() = PhotoEditorFragment.FragmentMode.CHOOSE_LABEL

    @Test
    fun alertDialogIsVisible() {
        /*
        Проверяю что окошко с выбором стикера показано. Потом нажимаю на выбор первого стикера, проверяю в toolbar  отобразилось или нет,
        затем тоже самое по очереди выбираю остальные стикеры
         */
        val titleId = mFragmentTestRule.activity.resources.getIdentifier("alertTitle", "id", "android")
        val title = onView(withId(titleId)).inRoot(isDialog())
        title.check(matches(withText(R.string.gs_photoeditor_lib_choose_stiker))).check(matches(isDisplayed()))

        labels.forEach {
            onView(withText(it.title)).check(matches(not(titleId))).perform(click())
            onView(withId(R.id.label_name)).check(matches(withText(it.title)))
            onView(withId(R.id.add_label_main_container)).perform(click())
            onView(withText(R.string.gs_photoeditor_lib_choose_stiker)).check(matches(isDisplayed()))
        }
    }


    @Test
    fun chooseLabelFirstSticker() {
        val titleId = mFragmentTestRule.activity.resources.getIdentifier("alertTitle", "id", "android")
        val title = onView(withId(titleId)).inRoot(isDialog())
        title.check(matches(withText(R.string.gs_photoeditor_lib_choose_stiker))).check(matches(isDisplayed()))
        val choosenLabel = labels[0]
        onView(withText(choosenLabel.title)).check(matches(not(titleId))).perform(click())
        onView(withId(R.id.label_name)).check(matches(withText(choosenLabel.title)))
        doneButtonClick()
        val resultLabel = mFragmentTestRule.activityResult.resultData.getParcelableExtra<Label>("INTENT_TAG_CHOSEN_LABEL")
        assert(mFragmentTestRule.activityResult.resultCode == RESULT_OK)
        assert(resultLabel.id == choosenLabel.id)
        assert(resultLabel.title == choosenLabel.title)
        assert(resultLabel.description == choosenLabel.description)
    }

    private fun doneButtonClick() {
        clickerFragment.doneFabButtonClick()
        assert(mFragmentTestRule.activityResult.resultData.data == getTestUri())
//        assert(mFragmentTestRule.activityResult.resultData.getParcelableExtra<Label>("INTENT_TAG_CHOSEN_LABEL") == getTestUri())
    }

    companion object {
        lateinit var labels: List<Label>
    }
}