package com.ahmedadeltito.photoeditor

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.rule.ActivityTestRule
import androidx.appcompat.app.AppCompatActivity
import org.junit.Rule

open class ClickerFragment<T: AppCompatActivity>(@get:Rule val activityRule: ActivityTestRule<T>) {

    open fun doneFabButtonClick() {
        onView(withId(R.id.fab_done)).perform(click())
    }
}