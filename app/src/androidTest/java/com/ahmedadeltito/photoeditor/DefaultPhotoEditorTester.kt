package com.ahmedadeltito.photoeditor

import android.os.SystemClock
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.NoMatchingViewException
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import android.view.View
import org.hamcrest.Matchers.not
import org.junit.Before
import org.junit.Rule
import org.junit.Test


/**
 * Тест для проверка фото редактора, который открывается без стикеров
 */

open class DefaultPhotoEditorTester : IniterFragment() {

    @get:Rule
    val mFragmentTestRule = FragmentTestRule(getTestFragment())

    @Before
    fun fragmentCanBeInstantiated() {
        mFragmentTestRule.launchActivity(null)
        onView(withId(R.id.parent)).check(matches(isDisplayed()))
    }

    @Test
    fun openEditor() {
        onView(withId(R.id.toolbar)).check(matches(isDisplayed()))
        onView(withId(R.id.fab_done)).check(matches(isDisplayed()))
        onView(withId(R.id.additional_group)).check(doesNotExist())
    }

    @Test
    fun painterModeOn() {
        onView(withId(R.id.brush_item_menu)).perform(click())
        SystemClock.sleep(500)
        try {
            onView(withId(R.id.add_caption_cl)).check(matches(not(isDisplayed())))
        } catch (e: NoMatchingViewException) {
            e.printStackTrace()
        }
        onView(withId(R.id.drawing_view_pickers_view)).check(matches(isDisplayed()))
        onView(withId(R.id.drawing_view_color_picker)).check(matches(isDisplayed()))
        //        onView(withId(R.id.drawing_view_color_picker)).check(matches(isDisplayed()));
        onView(withId(R.id.cancel_btn)).check(matches(isDisplayed()))
        onView(withId(R.id.done_btn)).check(matches(isDisplayed()))
        onView(withId(R.id.toolbar)).check(matches(not<View>(isDisplayed())))
        onView(withId(R.id.fab_done)).check(matches(not<View>(isDisplayed())))
    }

    private fun isPainterModeOff() {
        onView(withId(R.id.drawing_view_pickers_view)).check(matches(not<View>(isDisplayed())))
        onView(withId(R.id.drawing_view_color_picker)).check(matches(not<View>(isDisplayed())))
        onView(withId(R.id.cancel_btn)).check(matches(not<View>(isDisplayed())))
        onView(withId(R.id.done_btn)).check(matches(not<View>(isDisplayed())))
        onView(withId(R.id.toolbar)).check(matches(isDisplayed()))
        onView(withId(R.id.fab_done)).check(matches(isDisplayed()))
    }

    @Test
    fun painterModeOff() {
        painterModeOn()
        onView(withId(R.id.done_btn)).perform(click())
        isPainterModeOff()
    }

    @Test
    fun painterModeOffBackButton() {
        painterModeOn()
        Espresso.pressBack()
        isPainterModeOff()
    }

    @Test
    fun addTextModeOn() {
        //todo проверить видимость toolbar и done_btn
        onView(withId(R.id.text_item_menu)).perform(click())
        //        onView(withParent(withId(R.id.toolbar)))
        //        onView(withId(R.id.toolbar)).inRoot(withDecorView(not(is(getActivity().getWindow().getDecorView())))).check(matches(not(isDisplayed())));
        //        onView(withId(R.id.toolbar)).inRoot(withId(R.id.parent)).check(matches(not(isDisplayed())));
        //        onView(withId(R.id.fab_done)).check(matches(not(isDisplayed())));
        onView(withId(R.id.popup_window_rl)).check(matches(isDisplayed()))
        onView(withId(R.id.add_text_edit_text)).check(matches(isDisplayed()))
        onView(withId(R.id.color_picker_view_add_text_popup)).check(matches(isDisplayed()))
        onView(withId(R.id.drawing_view_color_picker)).check(matches(isDisplayed()))
        onView(withId(R.id.cancel_btn)).check(matches(isDisplayed()))
        onView(withId(R.id.done_btn)).check(matches(isDisplayed()))
        Espresso.closeSoftKeyboard()
    }

    @Test
    fun addTextModeOff() {
        addTextModeOn()
        onView(withId(R.id.done_btn)).perform(click())
        //        onView(withId(R.id.popup_window_rl)).check(matches(not(isDisplayed())));
        //        onView(withId(R.id.add_text_edit_text)).check(matches(not(isDisplayed())));
        //        onView(withId(R.id.color_picker_view_add_text_popup)).check(matches(not(isDisplayed())));
        //        onView(withId(R.id.drawing_view_color_picker)).check(matches(not(isDisplayed())));
        onView(withId(R.id.cancel_btn)).check(matches(not<View>(isDisplayed())))
        onView(withId(R.id.done_btn)).check(matches(not<View>(isDisplayed())))
        onView(withId(R.id.toolbar)).check(matches(isDisplayed()))
        onView(withId(R.id.fab_done)).check(matches(isDisplayed()))
    }

//    @Test
//    fun stressTest() {
//        openEditor()
//        for (i in 0..9) {
//            painterModeOff()
//            painterModeOffBackButton()
//            addTextModeOff()
//            println(i.toString())
//        }
//    }
}