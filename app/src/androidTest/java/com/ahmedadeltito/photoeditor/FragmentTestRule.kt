package com.ahmedadeltito.photoeditor

import androidx.test.rule.ActivityTestRule
import androidx.fragment.app.Fragment
import org.junit.Assert

open class FragmentTestRule(private val mFragment: androidx.fragment.app.Fragment) : ActivityTestRule<PhotoEditorActivity>(PhotoEditorActivity::class.java, true, false) {

    override fun afterActivityLaunched() {
        super.afterActivityLaunched()

        activity.runOnUiThread({
            try {
                //Instantiate and insert the fragment into the container layout
                val manager = activity.supportFragmentManager
                val transaction = manager.beginTransaction()
                transaction.replace(R.id.container, mFragment)
                transaction.commit()
            } catch (e: InstantiationException) {
                Assert.fail(String.format("%s: Could not insert %s into TestActivity: %s",
                        javaClass.simpleName,
                        mFragment::class.java.simpleName,
                        e.message))
            } catch (e: IllegalAccessException) {
                Assert.fail(String.format("%s: Could not insert %s into TestActivity: %s", javaClass.simpleName, mFragment::class.java.simpleName, e.message))
            }
        })
    }

}