package com.ahmedadeltito.photoeditor

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.location.Location
import android.net.Uri
import android.os.Environment
import androidx.fragment.app.Fragment
import android.view.View
import com.gradoservice.photoeditorviewlibrary.models.Label
import com.gradoservice.photoeditorviewlibrary.photoeditor.PhotoEditorFragment
import java.io.File
import java.util.*

open class IniterFragment {
    fun getTestFragment(): androidx.fragment.app.Fragment {
        val fragment = PhotoEditorFragment.newInstance(
                getTestBitmap(),
                getTestUri(),
                getTestLocation(),
                getTestDate(),
                getTestQuality(),
                getTestLabels(),
                getTestDefaultLabel(),
                getTestFragmentMode(),
                isNeedAddCaptionMode(),
                isRequiredCaption(),
                isRequiredStamp()
        )
        return fragment
    }

    open fun getTestBitmap(): Bitmap {
        val root = getTestUri().path
        val options = BitmapFactory.Options()
        options.inSampleSize = 1
        options.inMutable = true
        return BitmapFactory.decodeFile(root, options)
    }

    open fun getTestUri() = Uri.fromFile(File(Environment.getExternalStorageDirectory().path + "/DCIM/Camera/IMG_20190712_124632.jpg"))

    open fun getTestLocation(provider: String = "gps", lat: Double = 12.57436, lon: Double = 35.14234): Location {
        val location = Location(provider)
        location.latitude = lat
        location.longitude = lon
        return location
    }

    open fun getTestDate() = Calendar.getInstance().time

    open fun getTestQuality() = 80

    open fun getTestLabels(): List<Label>? = null

    open fun getTestDefaultLabel(): Label? = null

    open fun getTestFragmentMode(): PhotoEditorFragment.FragmentMode? = null

    open fun getTestCameraClick() = View.OnClickListener {}

    open fun isNeedAddCaptionMode() = true

    open fun isRequiredCaption() = false

    open fun isRequiredStamp() = false
}