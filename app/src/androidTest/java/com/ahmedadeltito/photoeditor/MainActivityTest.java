package com.ahmedadeltito.photoeditor;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import androidx.test.espresso.Espresso;
import androidx.test.rule.ActivityTestRule;
import androidx.fragment.app.Fragment;
import android.view.View;

import com.gradoservice.photoeditorviewlibrary.models.Label;
import com.gradoservice.photoeditorviewlibrary.photoeditor.PhotoEditorFragment;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.not;

public class MainActivityTest {
    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(
            MainActivity.class);

    @Before
    public void setUp() throws Exception {
        mActivityRule.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mActivityRule.getActivity().getButtons().setVisibility(View.GONE);
                mActivityRule.getActivity().getContainer().setVisibility(View.VISIBLE);
                final File root = new File(Environment.getExternalStorageDirectory().getPath() + "/DCIM/Camera/IMG_20190712_124632.jpg");
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 1;
                options.inMutable = true;
                Bitmap bitmap = BitmapFactory.decodeFile(root.getPath(), options);
                List<Label> labelList = new ArrayList<>();
                labelList.add(new Label(1, "1", "1"));
                labelList.add(new Label(2, "2", "2"));
                labelList.add(new Label(3, "3", "3"));
                labelList.add(new Label(4, "4", "4"));
                labelList.add(new Label(5, "5", "5"));

                Fragment fragment = PhotoEditorFragment.newInstance(
                        bitmap,
                        Uri.fromFile(root),
                        null,
                        new Date(System.currentTimeMillis()),
                        80,
                        labelList,
                        null,
                        null,
                        true,
                        false,
                        false
                );
                mActivityRule.getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container, fragment)
                        .commit();
            }
        });
    }

    /**
     * Открыть редактор в обычном режиме с lables
     */
    @Test
    public void openEditorDefault() {
        onView(withId(R.id.toolbar)).check(matches(isDisplayed()));
        onView(withId(R.id.fab_done)).check(matches(isDisplayed()));
        onView(withId(R.id.label_name)).check(matches(withText(R.string.gs_photoeditor_lib_without_sticker)));
    }

    @Test
    public void painterModeOn() {
        onView(withId(R.id.brush_item_menu)).perform(click());
        onView(withId(R.id.drawing_view_pickers_view)).check(matches(isDisplayed()));
        onView(withId(R.id.drawing_view_color_picker)).check(matches(isDisplayed()));
//        onView(withId(R.id.drawing_view_color_picker)).check(matches(isDisplayed()));
        onView(withId(R.id.cancel_btn)).check(matches(isDisplayed()));
        onView(withId(R.id.done_btn)).check(matches(isDisplayed()));
        onView(withId(R.id.toolbar)).check(matches(not(isDisplayed())));
        onView(withId(R.id.fab_done)).check(matches(not(isDisplayed())));
    }

    private void isPainterModeOff() {
        onView(withId(R.id.drawing_view_pickers_view)).check(matches(not(isDisplayed())));
        onView(withId(R.id.drawing_view_color_picker)).check(matches(not(isDisplayed())));
        onView(withId(R.id.cancel_btn)).check(matches(not(isDisplayed())));
        onView(withId(R.id.done_btn)).check(matches(not(isDisplayed())));
        onView(withId(R.id.toolbar)).check(matches(isDisplayed()));
        onView(withId(R.id.fab_done)).check(matches(isDisplayed()));
    }

    @Test
    public void painterModeOff() {
        painterModeOn();
        onView(withId(R.id.done_btn)).perform(click());
        isPainterModeOff();
    }

    @Test
    public void painterModeOffBackButton() {
        painterModeOn();
        Espresso.pressBack();
        isPainterModeOff();
    }

    @Test
    public void addTextModeOn() {
        //todo проверить видимость toolbar и done_btn
        onView(withId(R.id.text_item_menu)).perform(click());
//        onView(withParent(withId(R.id.toolbar)))
//        onView(withId(R.id.toolbar)).inRoot(withDecorView(not(is(getActivity().getWindow().getDecorView())))).check(matches(not(isDisplayed())));
//        onView(withId(R.id.toolbar)).inRoot(withId(R.id.parent)).check(matches(not(isDisplayed())));
//        onView(withId(R.id.fab_done)).check(matches(not(isDisplayed())));
        onView(withId(R.id.popup_window_rl)).check(matches(isDisplayed()));
        onView(withId(R.id.add_text_edit_text)).check(matches(isDisplayed()));
        onView(withId(R.id.color_picker_view_add_text_popup)).check(matches(isDisplayed()));
        onView(withId(R.id.drawing_view_color_picker)).check(matches(isDisplayed()));
        onView(withId(R.id.cancel_btn)).check(matches(isDisplayed()));
        onView(withId(R.id.done_btn)).check(matches(isDisplayed()));
        Espresso.closeSoftKeyboard();
    }

    @Test
    public void addTextModeOff() {
        addTextModeOn();
        onView(withId(R.id.done_btn)).perform(click());
//        onView(withId(R.id.popup_window_rl)).check(matches(not(isDisplayed())));
//        onView(withId(R.id.add_text_edit_text)).check(matches(not(isDisplayed())));
//        onView(withId(R.id.color_picker_view_add_text_popup)).check(matches(not(isDisplayed())));
//        onView(withId(R.id.drawing_view_color_picker)).check(matches(not(isDisplayed())));
        onView(withId(R.id.cancel_btn)).check(matches(not(isDisplayed())));
        onView(withId(R.id.done_btn)).check(matches(not(isDisplayed())));
        onView(withId(R.id.toolbar)).check(matches(isDisplayed()));
        onView(withId(R.id.fab_done)).check(matches(isDisplayed()));
    }

//    @Test
//    public void stressTest() {
//        openEditorDefault();
//        for (int i = 0; i < 10; i++) {
//            painterModeOff();
//            painterModeOffBackButton();
//            addTextModeOff();
//            System.out.println(String.valueOf(i));
//        }
//    }
}