package com.ahmedadeltito.photoeditor

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import com.ahmedadeltito.photoeditor.utils.getRandomInt
import com.ahmedadeltito.photoeditor.utils.getRandomSting
import com.gradoservice.photoeditorviewlibrary.models.Label
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class StartWithDefaultSticker : IniterFragment() {

    @get:Rule
    var mFragmentTestRule = FragmentTestRule(getTestFragment())

    val clickerFragment = ClickerFragment(mFragmentTestRule)
    lateinit var labels: List<Label>
    private lateinit var defLabel: Label

    @Before
    fun fragmentCanBeInstantiated() {
        mFragmentTestRule.launchActivity(null)
    }

    override fun getTestLabels(): List<Label>? {
        val size = getRandomInt()
        val list = mutableListOf<Label>()
        for (i in 1..size) {
            list.add(Label(i.toLong(), getRandomSting(), getRandomSting()))
        }
        labels = list
        return list
    }


    override fun getTestDefaultLabel(): Label? {
        defLabel = labels[0]
        return defLabel
    }

    @Test
    fun startWithDefaultSticker() {
        onView(ViewMatchers.withId(R.id.label_name)).check(ViewAssertions.matches(ViewMatchers.withText(defLabel.title)))
    }
}