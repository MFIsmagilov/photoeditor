package com.ahmedadeltito.photoeditor.utils

import net.bytebuddy.utility.RandomString
import java.util.*

fun getRandomSting(): String = RandomString.make(getRandomInt())

fun getRandomInt(): Int {
    val max = 5
    val min = 1
    return Random().nextInt(max - min + 1) + min
}
