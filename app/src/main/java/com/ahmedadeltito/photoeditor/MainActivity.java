package com.ahmedadeltito.photoeditor;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.gradoservice.photoeditorviewlibrary.common.widget.AndroidUtilities;
import com.gradoservice.photoeditorviewlibrary.models.Label;
import com.gradoservice.photoeditorviewlibrary.photoeditor.PhotoEditorFragment;
import com.gradoservice.photoeditorviewlibrary.signaturepad.SignaturePadStarter;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class MainActivity extends MediaActivity {

    private Fragment fragment;


    private LinearLayout buttons;
    private FrameLayout container;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        AndroidUtilities.setDensity(metrics.density);
        buttons = (LinearLayout) findViewById(R.id.buttons);
        container = (FrameLayout) findViewById(R.id.container);
        final File root = new File(Environment.getExternalStorageDirectory().getPath() + "/DCIM/Camera/IMG_20190712_124632.jpg");
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 1;
        options.inMutable = true;
        Bitmap bitmap = BitmapFactory.decodeFile(root.getPath(), options);
        Label[] labelList = createLabels();
        fragment = PhotoEditorFragment.newInstance(
                Uri.fromFile(root),
                new Date(System.currentTimeMillis()),
                80,
                labelList,
                labelList[0],
                null,
                true,
                false,
                false
        );

    }

    public void openSignaturePad(View view) {
        new SignaturePadStarter(this)
                .setTheme(R.style.AppTheme)
                .startActivityForResult(102);
    }

    public void openEditor(View view) {
        buttons.setVisibility(View.GONE);
        container.setVisibility(View.VISIBLE);

        getSupportFragmentManager()
                .beginTransaction()
//                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
//                .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                .replace(R.id.root, fragment)
                .commit();
    }


    private Location createTestLocation() {
        Location location = new Location("gps");
        location.setLatitude(12.57436);
        location.setLongitude(35.14234);
        return null;
    }

    private Label[] createLabels() {
        List<Label> labelList = new ArrayList<>();
        labelList.add(new Label(1, "1", "1"));
        labelList.add(new Label(2, "2", "2"));
        labelList.add(new Label(3, "3", "3"));
        labelList.add(new Label(4, "4", "4"));
        labelList.add(new Label(5, "5", "5"));
        return labelList.toArray(new Label[labelList.size()]);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null) {
            Log.d("MainActivty", resultCode + " " + resultCode + " " + data.getData());
        }
    }

    public void openUserGallery(View view) {
        openGallery();
    }

    public void openUserCamera(View view) {
        startCameraActivity();
    }

    @Override
    protected void onPhotoTaken() {
    }


    @Override
    protected void onPause() {
        super.onPause();
        if (getSupportFragmentManager().getFragments().size() > 0)
            MyApp.get().mustDie(getSupportFragmentManager().getFragments().get(0));
    }

    public LinearLayout getButtons() {
        return buttons;
    }

    public FrameLayout getContainer() {
        return container;
    }

}
