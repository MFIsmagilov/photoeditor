package com.ahmedadeltito.photoeditor

import android.os.StrictMode
import android.widget.Toast
import androidx.multidex.MultiDexApplication
import com.squareup.leakcanary.LeakCanary
import com.squareup.leakcanary.RefWatcher


class MyApp : MultiDexApplication() {

    private lateinit var refWatcher: RefWatcher

    init {
        instance = this
    }

    override fun onCreate() {
        super.onCreate()
        setupLeakCanary()
    }

    fun mustDie(obj: Any) {
        refWatcher.watch(obj)
    }

    private fun setupLeakCanary() {
        if (LeakCanary.isInAnalyzerProcess(this)) {
            Toast.makeText(this, "isInAnalyzerProcess", Toast.LENGTH_LONG).show()
            return
        }
        enabledStrictMode()
        refWatcher = LeakCanary.install(this)
    }

    private fun enabledStrictMode() {
        StrictMode.setThreadPolicy(StrictMode.ThreadPolicy.Builder()
                .detectAll()
                .penaltyLog()
                .penaltyDeath()
                .build())
    }

    companion object {
        private lateinit var instance: MyApp

        @JvmStatic
        fun get() = instance

    }
}