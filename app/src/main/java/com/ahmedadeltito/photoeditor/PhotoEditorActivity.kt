package com.ahmedadeltito.photoeditor

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.gradoservice.photoeditorviewlibrary.common.widget.AndroidUtilities

class PhotoEditorActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photo_editor)
        val metrics = resources.displayMetrics
        AndroidUtilities.setDensity(metrics.density)
    }


    companion object {
        fun getIntent(context: Context): Intent {
            val intent = Intent(context, PhotoEditorActivity::class.java)
            return intent;
        }
    }
}
