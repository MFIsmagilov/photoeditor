package com.gradoservice.photoeditorviewlibrary.common;

import com.gradoservice.photoeditorviewlibrary.models.Label;

public interface PhotoEditorFinishingCallback {

    void onSuccess(Label tempLabel, String caption);

    void onFailure(Throwable throwable);

}
