package com.gradoservice.photoeditorviewlibrary.common.fragment;

import androidx.fragment.app.Fragment;

import com.ahmedadeltito.photoeditorsdk.PhotoEditorSDK;

/**
 * Created by maratismagilov on 19.03.2018.
 */

public class BaseEditorFragment extends Fragment {

    protected PhotoEditorSDK photoEditorSDK;

    public BaseEditorFragment() {

    }
}
