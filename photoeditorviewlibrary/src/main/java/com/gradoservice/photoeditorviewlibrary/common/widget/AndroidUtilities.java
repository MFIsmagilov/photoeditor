package com.gradoservice.photoeditorviewlibrary.common.widget;

/**
 * Created by maratismagilov on 26.02.2018.
 */

public class AndroidUtilities {
    public static float density = 1;

    public static void setDensity(float d){ //todo: fixme
        density = d;
    }

    public static int dp(float value) {
        if (value == 0) {
            return 0;
        }
        return (int) Math.ceil(density * value);
    }

}
