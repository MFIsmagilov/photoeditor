package com.gradoservice.photoeditorviewlibrary.common.widget;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.gradoservice.photoeditorviewlibrary.R;

import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Created by maratismagilov on 26.02.2018.
 */

public class ColorPicker extends FrameLayout {
    private static final int[] COLORS = new int[]{
            0xffea2739,
            0xffdb3ad2,
            0xff3051e3,
            0xff49c5ed,
            0xff80c864,
            0xfffcde65,
            0xfffc964d,
            0xff000000,
            0xffffffff
    };
    private static final float[] LOCATIONS = new float[]{
            0.0f,
            0.14f,
            0.24f,
            0.39f,
            0.49f,
            0.62f,
            0.73f,
            0.85f,
            1.0f
    };
    private final BehaviorSubject<ColorPicker.Brush> selectedColorSubject = BehaviorSubject.create();
    private ColorPicker.ColorPickerDelegate delegate;
    private boolean interacting;
    private boolean changingWeight;
    private boolean wasChangingWeight;
    private OvershootInterpolator interpolator = new OvershootInterpolator(1.02F);
    private ImageView settingsButton;
    private ImageView undoButton;
    private Drawable shadowDrawable;
    private Paint gradientPaint = new Paint(1);
    private Paint backgroundPaint = new Paint(1);
    private Paint swatchPaint = new Paint(1);
    private Paint swatchStrokePaint = new Paint(1);
    private RectF rectF = new RectF();
    private float location = 1.0F;
    private float weight = 0.27F;
    private float draggingFactor;
    private boolean dragging;
    private Integer selectedColor = 0;

    public ColorPicker(Context context) {
        super(context);
        this.init(context);
    }

    public ColorPicker(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.init(context);
    }

    public ColorPicker(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.init(context);
    }

    @RequiresApi(
            api = 21
    )
    public ColorPicker(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.init(context);
    }

    private static int getSize(float size) {
        return (int)(size < 0.0F ? size : (float)AndroidUtilities.dp(size));
    }

    private void init(Context context) {
        this.setWillNotDraw(false);
        this.shadowDrawable = this.getResources().getDrawable(R.drawable.gs_photoeditor_lib_knob_shadow);
        this.backgroundPaint.setColor(-1);
        this.swatchStrokePaint.setStyle(Paint.Style.STROKE);
        this.swatchStrokePaint.setStrokeWidth((float)AndroidUtilities.dp(1.0F));
        this.settingsButton = new ImageView(context);
        this.settingsButton.setVisibility(View.GONE);
        this.settingsButton.setScaleType(ImageView.ScaleType.CENTER);
        this.settingsButton.setImageResource(R.drawable.gs_photoeditor_lib_ic_brush);
        this.addView(this.settingsButton, new LayoutParams(60, 52));
        this.settingsButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (ColorPicker.this.delegate != null) {
                    ColorPicker.this.delegate.onSettingsPressed();
                }

            }
        });
        this.undoButton = new ImageView(context);
        this.undoButton.setVisibility(View.GONE);
        this.undoButton.setScaleType(ImageView.ScaleType.CENTER);
        this.undoButton.setImageResource(R.drawable.gs_photoeditor_lib_ic_undo);
        this.addView(this.undoButton, new LayoutParams(60, 52));
        this.undoButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (ColorPicker.this.delegate != null) {
                    ColorPicker.this.delegate.onUndoPressed();
                }

            }
        });
        this.location = context.getSharedPreferences("paint", 0).getFloat("last_color_location", 1.0F);
        this.setLocation(this.location);
    }

    public void setUndoEnabled(boolean enabled) {
        this.undoButton.setAlpha(enabled ? 1.0F : 0.3F);
        this.undoButton.setEnabled(enabled);
    }

    public void setDelegate(ColorPicker.ColorPickerDelegate colorPickerDelegate) {
        this.delegate = colorPickerDelegate;
    }

    public View getSettingsButton() {
        return this.settingsButton;
    }

    public void setSettingsButtonImage(int resId) {
        this.settingsButton.setImageResource(resId);
    }

    public Swatch getSwatch() {
        return new Swatch(this.colorForLocation(this.location), this.location, this.weight);
    }

    public void setSwatch(Swatch swatch) {
        this.setLocation(swatch.colorLocation);
        this.setWeight(swatch.brushWeight);
    }

    public int colorForLocation(float location) {
        if (location <= 0.0F) {
            return COLORS[0];
        } else if (location >= 1.0F) {
            return COLORS[COLORS.length - 1];
        } else {
            int leftIndex = -1;
            int rightIndex = -1;

            for(int i = 1; i < LOCATIONS.length; ++i) {
                float value = LOCATIONS[i];
                if (value > location) {
                    leftIndex = i - 1;
                    rightIndex = i;
                    break;
                }
            }

            float leftLocation = LOCATIONS[leftIndex];
            int leftColor = COLORS[leftIndex];
            float rightLocation = LOCATIONS[rightIndex];
            int rightColor = COLORS[rightIndex];
            float factor = (location - leftLocation) / (rightLocation - leftLocation);
            return this.interpolateColors(leftColor, rightColor, factor);
        }
    }

    private int interpolateColors(int leftColor, int rightColor, float factor) {
        factor = Math.min(Math.max(factor, 0.0F), 1.0F);
        int r1 = Color.red(leftColor);
        int r2 = Color.red(rightColor);
        int g1 = Color.green(leftColor);
        int g2 = Color.green(rightColor);
        int b1 = Color.blue(leftColor);
        int b2 = Color.blue(rightColor);
        int r = Math.min(255, (int)((float)r1 + (float)(r2 - r1) * factor));
        int g = Math.min(255, (int)((float)g1 + (float)(g2 - g1) * factor));
        int b = Math.min(255, (int)((float)b1 + (float)(b2 - b1) * factor));
        return Color.argb(255, r, g, b);
    }

    public void setLocation(float value) {
        int color = this.colorForLocation(this.location = value);
        this.swatchPaint.setColor(color);
        float[] hsv = new float[3];
        Color.colorToHSV(color, hsv);
        if ((double)hsv[0] < 0.001D && (double)hsv[1] < 0.001D && hsv[2] > 0.92F) {
            int c = (int)((1.0F - (hsv[2] - 0.92F) / 0.08F * 0.22F) * 255.0F);
            this.swatchStrokePaint.setColor(Color.rgb(c, c, c));
        } else {
            this.swatchStrokePaint.setColor(color);
        }

        this.invalidate();
    }

    public void setWeight(float value) {
        this.weight = value;
        this.invalidate();
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (event.getPointerCount() > 1) {
            return false;
        } else {
            float x = event.getX() - this.rectF.left;
            float y = event.getY() - this.rectF.top;
            if (!this.interacting && y < (float)(-AndroidUtilities.dp(10.0F))) {
                return false;
            } else {
                int action = event.getActionMasked();
                if (action != 3 && action != 1 && action != 6) {
                    if (action == 0 || action == 2) {
                        if (!this.interacting) {
                            this.interacting = true;
                            if (this.delegate != null) {
                                this.delegate.onBeganColorPicking();
                            }
                        }

                        float colorLocation = Math.max(0.0F, Math.min(1.0F, x / this.rectF.width()));
                        this.setLocation(colorLocation);
                        this.setDragging(true, true);
                        if (y < (float)(-AndroidUtilities.dp(10.0F))) {
                            this.changingWeight = true;
                            float weightLocation = (-y - (float)AndroidUtilities.dp(10.0F)) / (float)AndroidUtilities.dp(190.0F);
                            weightLocation = Math.max(0.0F, Math.min(1.0F, weightLocation));
                            this.setWeight(weightLocation);
                        }

                        if (this.delegate != null) {
                            this.delegate.onColorValueChanged();
                        }

                        this.selectedColorSubject.onNext(new ColorPicker.Brush(this.colorForLocation(this.location), this.weight));
                        return true;
                    }
                } else {
                    if (this.interacting) {
                        this.getContext().getSharedPreferences("paint", 0).edit().putFloat("last_color_location", this.location).apply();
                    }

                    this.interacting = false;
                    this.wasChangingWeight = this.changingWeight;
                    this.changingWeight = false;
                    this.setDragging(false, true);
                }

                return false;
            }
        }
    }

    @SuppressLint({"DrawAllocation"})
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        int width = right - left;
        int height = bottom - top;
        this.gradientPaint.setShader(new LinearGradient((float)AndroidUtilities.dp(56.0F), 0.0F, (float)(width - AndroidUtilities.dp(56.0F)), 0.0F, COLORS, LOCATIONS, Shader.TileMode.REPEAT));
        int y = height - AndroidUtilities.dp(32.0F);
        this.rectF.set((float)AndroidUtilities.dp(56.0F), (float)y, (float)(width - AndroidUtilities.dp(56.0F)), (float)(y + AndroidUtilities.dp(12.0F)));
        this.settingsButton.layout(width - this.settingsButton.getMeasuredWidth(), height - AndroidUtilities.dp(52.0F), width, height);
        this.undoButton.layout(0, height - AndroidUtilities.dp(52.0F), this.settingsButton.getMeasuredWidth(), height);
    }

    protected void onDraw(Canvas canvas) {
        canvas.drawRoundRect(this.rectF, (float)AndroidUtilities.dp(6.0F), (float)AndroidUtilities.dp(6.0F), this.gradientPaint);
        int cx = (int)(this.rectF.left + this.rectF.width() * this.location);
        int cy = (int)(this.rectF.centerY() + this.draggingFactor * (float)(-AndroidUtilities.dp(70.0F)) - (this.changingWeight ? this.weight * (float)AndroidUtilities.dp(190.0F) : 0.0F));
        int side = (int)((float)AndroidUtilities.dp(24.0F) * 0.5F * (1.0F + this.draggingFactor));
        this.shadowDrawable.setBounds(cx - side, cy - side, cx + side, cy + side);
        this.shadowDrawable.draw(canvas);
        float swatchRadius = (float)((int)Math.floor((double)((float)AndroidUtilities.dp(4.0F) + (float)(AndroidUtilities.dp(19.0F) - AndroidUtilities.dp(4.0F)) * this.weight))) * (1.0F + this.draggingFactor) / 2.0F;
        canvas.drawCircle((float)cx, (float)cy, (float)(AndroidUtilities.dp(22.0F) / 2) * (this.draggingFactor + 1.0F), this.backgroundPaint);
        canvas.drawCircle((float)cx, (float)cy, swatchRadius, this.swatchPaint);
        canvas.drawCircle((float)cx, (float)cy, swatchRadius - (float)AndroidUtilities.dp(0.5F), this.swatchStrokePaint);
    }

    public float getDraggingFactor() {
        return this.draggingFactor;
    }

    private void setDraggingFactor(float factor) {
        this.draggingFactor = factor;
        this.invalidate();
    }

    private void setDragging(boolean value, boolean animated) {
        if (this.dragging != value) {
            this.dragging = value;
            float target = this.dragging ? 1.0F : 0.0F;
            if (animated) {
                Animator a = ObjectAnimator.ofFloat(this, "draggingFactor", new float[]{this.draggingFactor, target});
                a.setInterpolator(this.interpolator);
                int duration = 300;
                if (this.wasChangingWeight) {
                    duration = (int)((float)duration + this.weight * 75.0F);
                }

                a.setDuration((long)duration);
                a.start();
            } else {
                this.setDraggingFactor(target);
            }

        }
    }

    public Observable<ColorPicker.Brush> getBrush() {
        return this.selectedColorSubject;
    }

    public interface ColorPickerDelegate {
        void onBeganColorPicking();

        void onColorValueChanged();

        void onFinishedColorPicking();

        void onSettingsPressed();

        void onUndoPressed();
    }

    public class Brush {
        private int color;
        private float size;

        public Brush(int color, float size) {
            this.color = color;
            this.size = size;
        }

        public int getColor() {
            return this.color;
        }

        public float getSize() {
            return this.size * 100.0F;
        }
    }
}