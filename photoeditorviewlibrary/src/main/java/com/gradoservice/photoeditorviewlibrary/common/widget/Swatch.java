package com.gradoservice.photoeditorviewlibrary.common.widget;

/**
 * Created by maratismagilov on 22.03.2018.
 */

public class Swatch {

    public int color;
    public float colorLocation;
    public float brushWeight;

    public Swatch(int color, float colorLocation, float brushWeight) {
        this.color = color;
        this.colorLocation = colorLocation;
        this.brushWeight = brushWeight;
    }
}
