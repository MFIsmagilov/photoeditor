package com.gradoservice.photoeditorviewlibrary.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Label implements Parcelable {
    public static final Creator<Label> CREATOR = new Creator<Label>() {
        public Label createFromParcel(Parcel source) {
            return new Label(source);
        }

        public Label[] newArray(int size) {
            return new Label[size];
        }
    };
    private long id;
    private String title;
    private String description;

    public Label(long id, String title, String description) {
        this.id = id;
        this.title = title;
        this.description = description;
    }

    public Label() {
    }

    protected Label(Parcel in) {
        this.id = in.readLong();
        this.title = in.readString();
        this.description = in.readString();
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.title);
        dest.writeString(this.description);
    }

    public boolean isCorrectly() {
        return this.id > 0L;
    }
}
