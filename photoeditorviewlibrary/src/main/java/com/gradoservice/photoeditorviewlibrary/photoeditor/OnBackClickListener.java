package com.gradoservice.photoeditorviewlibrary.photoeditor;

/**
 * Кнопка назад в ActionBar
 */

public interface OnBackClickListener {
    void onBackClick();
}
