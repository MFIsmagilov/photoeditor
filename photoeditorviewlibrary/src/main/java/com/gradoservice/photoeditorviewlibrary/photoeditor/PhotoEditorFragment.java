package com.gradoservice.photoeditorviewlibrary.photoeditor;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.ShareActionProvider;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.MenuItemCompat;
import androidx.fragment.app.Fragment;
import androidx.transition.Transition;
import androidx.transition.TransitionManager;
import androidx.transition.TransitionSet;

import com.ahmedadeltito.photoeditorsdk.BrushDrawingView;
import com.ahmedadeltito.photoeditorsdk.OnPhotoEditorSDKListener;
import com.ahmedadeltito.photoeditorsdk.PhotoEditorSDK;
import com.ahmedadeltito.photoeditorsdk.ViewType;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.gradoservice.photoeditorviewlibrary.R;
import com.gradoservice.photoeditorviewlibrary.common.KeyboardHeightProvider;
import com.gradoservice.photoeditorviewlibrary.common.PhotoEditorFinishingCallback;
import com.gradoservice.photoeditorviewlibrary.common.fragment.BaseEditorFragment;
import com.gradoservice.photoeditorviewlibrary.common.widget.AndroidUtilities;
import com.gradoservice.photoeditorviewlibrary.common.widget.ColorPicker;
import com.gradoservice.photoeditorviewlibrary.models.Label;
import com.gradoservice.photoeditorviewlibrary.utils.AnimateUtils;
import com.gradoservice.photoeditorviewlibrary.utils.ApplySchedulers;
import com.gradoservice.photoeditorviewlibrary.utils.BitmapUtils;
import com.gradoservice.photoeditorviewlibrary.utils.StringUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.subjects.BehaviorSubject;

import static android.app.Activity.RESULT_OK;

/**
 * Created by maratismagilov on 21.03.2018.
 */
//fixme: кажется я тут перемудрил, надо сделать лучше
public class PhotoEditorFragment extends BaseEditorFragment implements OnPhotoEditorSDKListener {


    public static String INTENT_TAG_CHOSEN_LABEL = "INTENT_TAG_CHOSEN_LABEL";
    public static String INTENT_TAG_CAPTION = "INTENT_TAG_CAPTION";

    private static String OUTPUT_PATH = "OUTPUT_PATH";
    private static String LOCATION = "LOCATION";
    private static String DATE = "DATE";
    private static String QUALITY = "QUALITY";
    private static String LABELS = "LABELS";
    private static String DEFAULT_LABEL = "DEFAULT_LABEL";
    private static String MODE = "MODE";
    private static String NEED_ADD_CAPTION_MODE = "NEED_ADD_CAPTION_MODE";
    private static String REQUIRED_CAPTION = "REQUIRED_CAPTION";
    private static String REQUIRED_STAMP = "REQUIRED_STAMP";

    private static String USER_CHOOSED_STICKER = "USER_CHOOSE_STICKER";


    private final CompositeDisposable disposables = new CompositeDisposable();
    private final BehaviorSubject<PhotoEditorFragment.FragmentMode> currentFragmentModeSubject = BehaviorSubject.create();
    private final BehaviorSubject<Integer> visibilitySubject = BehaviorSubject.create();
    PopupWindow pop;
    private Uri outputPath;
    private Location location;
    private Date date;
    private int quality;
    private List<Label> labels;

    private RelativeLayout mLayout;
    private RelativeLayout colorPickerView;
    private ColorPicker colorPicker;
    private RelativeLayout doneAreaRl;
    private ProgressBar doneFabProgress;
    private Button donePaintBtn;
    private Button cancelPaintBtn;
    private Observable transitionObservable;
    private RelativeLayout relativeLayoutPopWindow;
    private String editTextChangeString = "";
    private int editTextChangeColorCode = -1;
    private PhotoEditorFragment.PainterMode painterMode;
    private PhotoEditorFragment.ChangeMode changeMode;
    private PhotoEditorFragment.AddTextMode addTextMode;
    private PhotoEditorFragment.ChooseLabelMode chooseLabelMode;
    private PhotoEditorFragment.AddCaptionMode addCaptionMode;
    private Label chosenLabel;
    private boolean isNeedAddCaptionMode = true;
    private boolean isRequiredCaption = true;


    private boolean isNeedStamp = true;
    private PhotoEditorFragment.FragmentMode modeAtStart;
    private TextView labelNameTv;
    private KeyboardHeightProvider keyboardHeightProvider;

    private ShareActionProvider shareActionProvider; //для кнопки поделиться
    private View view;

    private Integer originalSoftMode = null;


    public PhotoEditorFragment() {
        super();
    }

    public void parseArguments(Bundle bundle) {
        setOutputPath(bundle.getParcelable(OUTPUT_PATH));
        setLocation(bundle.getParcelable(LOCATION));
        setDate(new Date(bundle.getLong(DATE)));
        setQuality(bundle.getInt(QUALITY));
        setChosenLabel(bundle.getParcelable(DEFAULT_LABEL));
        setModeAtStart(FragmentMode.valueOf(bundle.getInt(MODE)));
        setNeedAddCaptionMode(bundle.getBoolean(NEED_ADD_CAPTION_MODE));
        setRequiredCaption(bundle.getBoolean(REQUIRED_CAPTION));
        setNeedStamp(bundle.getBoolean(REQUIRED_STAMP));
        Parcelable[] labelArray = bundle.getParcelableArray(LABELS);
        Label incorrectLabel = new Label(-1, getString(R.string.gs_photoeditor_lib_without_sticker), getString(R.string.gs_photoeditor_lib_without_sticker));
        if (labelArray != null) {
            ArrayList<Label> labels = null;
            if (labelArray instanceof Label[]) {
                labels = new ArrayList<Label>(Arrays.asList((Label[]) labelArray));
            } else {
                labels = new ArrayList<>(labelArray.length);
                for (int i = 0; i < labelArray.length; i++) {
                    labels.add((Label) labelArray[i]);
                }
            }
            labels.add(incorrectLabel);
            if (chosenLabel == null) {
                chosenLabel = incorrectLabel;
            }
            setLabels(labels);
        }
    }

    /**
     * @param date                 - время для штампа
     * @param quality              - качество 0 - 100
     * @param labels               - если не null то появится возможность выбора MapMobile-sticker
     * @param defaultLabel         - стикер по умолчанию, если null то будет отображаться "Нет стикера"
     * @param mode                 - фоторедактор имеет режимы: @see PhotoEditorFragment#FragmentMode
     * @param isNeedAddCaptionMode - если true то будет отображено поле "Добавить подпись"
     * @param isRequiredCaption    - если true то будет требоваться добавить значение в поле "Добавить подпись"
     * @param isNeedStamp          - если true и date != null то на фотографию будет поставлен штамп со временем
     * @return фрагмент PhotoEditorFragment
     */
    public static Fragment newInstance(
            @NonNull Uri outputPath,
//            @Nullable Location location,
            @Nullable Date date,
            int quality,
            @Nullable Label[] labels,
            @Nullable Label defaultLabel,
            @Nullable PhotoEditorFragment.FragmentMode mode,
            boolean isNeedAddCaptionMode,
            boolean isRequiredCaption,
            boolean isNeedStamp) {
        Bundle args = new Bundle();
        args.putParcelable(OUTPUT_PATH, outputPath);
//        args.putParcelable(LOCATION, location);

        if (date != null)
            args.putLong(DATE, date.getTime());

        args.putInt(QUALITY, quality);
        args.putParcelableArray(LABELS, labels);
        args.putParcelable(DEFAULT_LABEL, defaultLabel);

        if (mode != null)
            args.putInt(MODE, mode.ordinal());

        args.putBoolean(NEED_ADD_CAPTION_MODE, isNeedAddCaptionMode);
        args.putBoolean(REQUIRED_CAPTION, isRequiredCaption);
        args.putBoolean(REQUIRED_STAMP, isNeedStamp);
        PhotoEditorFragment fragment = new PhotoEditorFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void setNeedStamp(boolean needStamp) {
        isNeedStamp = needStamp;
    }

    public boolean isNeedStamp() {
        return isNeedStamp;
    }

    public void setNeedAddCaptionMode(boolean needAddCaptionMode) {
        isNeedAddCaptionMode = needAddCaptionMode;
    }

    public void setRequiredCaption(boolean requiredCaption) {
        isRequiredCaption = requiredCaption;
    }

    public void setModeAtStart(PhotoEditorFragment.FragmentMode modeAtStart) {
        this.modeAtStart = modeAtStart;
    }

    public void setChosenLabel(Label chosenLabel) {
        this.chosenLabel = chosenLabel;
    }

    public void setQuality(int quality) {
        this.quality = quality;
    }

    public void setLabels(List<Label> labels) {
        this.labels = labels;
    }

    public void setOutputPath(Uri outputPath) {
        this.outputPath = outputPath;
    }

    public Uri getOutputPath() {
        return this.outputPath;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public void setDate(Date date) {
        this.date = date;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        android.os.Debug.waitForDebugger();
        parseArguments(getArguments());
        originalSoftMode = getActivity().getWindow().getAttributes().softInputMode;
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        AndroidUtilities.setDensity(getActivity().getResources().getDisplayMetrics().density);
        view = inflater.inflate(R.layout.gs_photoeditor_lib_new_ed, container, false);
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setTitle("");
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayUseLogoEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
        setHasOptionsMenu(true);
        mLayout = (RelativeLayout) view.findViewById(R.id.parent);
        RelativeLayout parentImageRelativeLayout = (RelativeLayout) view.findViewById(R.id.parent_image_rl);
        FloatingActionButton deleteView = (FloatingActionButton) view.findViewById(R.id.fab_recyclerbin);
        ImageView photoEditImageView = (ImageView) view.findViewById(R.id.photo_edit_iv);
        BrushDrawingView brushDrawingView = (BrushDrawingView) view.findViewById(R.id.drawing_view);
        colorPickerView = (RelativeLayout) view.findViewById(R.id.drawing_view_pickers_view);
        colorPicker = (ColorPicker) view.findViewById(R.id.drawing_view_color_picker);
        FloatingActionButton recyclerBinFabBtn = (FloatingActionButton) view.findViewById(R.id.fab_recyclerbin);
        doneAreaRl = (RelativeLayout) view.findViewById(R.id.done_area_rl);
        FloatingActionButton doneBtn = (FloatingActionButton) view.findViewById(R.id.fab_done);
        doneFabProgress = (ProgressBar) view.findViewById(R.id.fab_progress);
        FloatingActionButton cameraFabBtn = (FloatingActionButton) view.findViewById(R.id.fab_camera);
        cameraFabBtn.hide();
        donePaintBtn = (Button) view.findViewById(R.id.done_btn);
        cancelPaintBtn = (Button) view.findViewById(R.id.cancel_btn);
        photoEditorSDK = (new PhotoEditorSDK.PhotoEditorSDKBuilder(getActivity())).parentView(parentImageRelativeLayout).childView(photoEditImageView).deleteView(deleteView).brushDrawingView(brushDrawingView).buildPhotoEditorSDK();
        photoEditorSDK.setOnPhotoEditorSDKListener(this);

        Disposable disposable = Observable.fromCallable(
                () -> {
                    final File root = new File(getOutputPath().getPath());
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 1;
                    options.inMutable = true;
                    return BitmapFactory.decodeFile(root.getPath(), options);
                })
                .compose(new ApplySchedulers<>())
                .subscribe(photoEditImageView::setImageBitmap);

        disposables.add(disposable);
        //fixme без этой строчки на Xiaomi черный экран, но почему
        photoEditImageView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        photoEditImageView.invalidate();
        return view;
    }

    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        painterMode = new PainterMode();
        changeMode = new ChangeMode();
        addTextMode = new AddTextMode();
        if (isNeedAddCaptionMode) {
            addCaptionMode = new AddCaptionMode();
        }
        if (labels != null) {
            chooseLabelMode = new ChooseLabelMode();
        }
    }

    public void onStart() {
        super.onStart();
        View view = getView();
        if (view != null)
            view.setOnKeyListener(new View.OnKeyListener() {
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    Log.d("KEY", keyCode + " " + 4);
                    if (event.getAction() != 0) {
                        return false;
                    } else {
                        if (event.getAction() == 0 && keyCode == 4 && currentFragmentModeSubject.getValue() != PhotoEditorFragment.FragmentMode.DEFAULT) {
                            currentFragmentModeSubject.onNext(PhotoEditorFragment.FragmentMode.DEFAULT);
                        } else if (getActivity() != null) {
                            getActivity().finish();
                        }
                        return true;
                    }
                }
            });
        initColorPicker();
        initDefaultColors();
        setClickListenersForFabButton();
        disposables.add(currentFragmentModeSubject.subscribe(changeMode.getConsumer()));
        disposables.add(currentFragmentModeSubject.subscribe(painterMode.getConsumer()));
        disposables.add(currentFragmentModeSubject.subscribe(addTextMode.getConsumer()));
        if (labels != null) {
            disposables.add(currentFragmentModeSubject.subscribe(chooseLabelMode.getConsumer()));
        }
        if (addCaptionMode != null) {
            disposables.add(currentFragmentModeSubject.subscribe(addCaptionMode.getConsumer()));
            disposables.add(visibilitySubject.subscribe(addCaptionMode.getTransitionsConsumer()));
        }

        disposables.add(visibilitySubject.subscribe(changeMode.getTransitionsConsumer()));
        disposables.add(visibilitySubject.subscribe(painterMode.getTransitionsConsumer()));
        disposables.add(visibilitySubject.subscribe(addTextMode.getTransitionsConsumer()));

        List<Observable> transitionObservableList = new ArrayList<>();

        if (painterMode != null)
            transitionObservableList.add(painterMode.getTransitionObservable());
        if (changeMode != null)
            transitionObservableList.add(changeMode.getTransitionObservable());
        if (addTextMode != null)
            transitionObservableList.add(addTextMode.getTransitionObservable());
        if (addCaptionMode != null)
            transitionObservableList.add(addCaptionMode.getTransitionObservable());

        transitionObservable = TransitionsObservable.add(transitionObservableList.toArray(new Observable[transitionObservableList.size()]));
        disposables.add(transitionObservable.subscribe(new Consumer<TransitionSet>() {
            //собрал все анимашки, применил их, теперь можно делать visible true/false у fragmentmode для этого
            //оповещаем всех подписчиков
            public void accept(TransitionSet transitionSet) throws Exception {
                if (transitionSet.getTransitionCount() != 0) {
                    TransitionManager.endTransitions(mLayout);
                    TransitionManager.beginDelayedTransition(mLayout, transitionSet);
                    visibilitySubject.onNext(1);
                }

            }
        }));
        if (modeAtStart != null) {
            currentFragmentModeSubject.onNext(modeAtStart);
        }
    }


    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if (chosenLabel != null)
            outState.putParcelable(USER_CHOOSED_STICKER, chosenLabel);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            Label label = savedInstanceState.getParcelable(USER_CHOOSED_STICKER);
            if (label != null)
                setChosenLabel(label);
        }
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(com.gradoservice.photoeditorviewlibrary.R.menu.toolbar_menu, menu);
        //Задача #45480, пока сделаю просто visible = false, в дальнейшем можно смело выпиливать
        MenuItem item = menu.findItem(R.id.share_item_menu);
        shareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(item);
        item.getActionView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareImage();
            }
        });
        if (labels != null && !labels.isEmpty()) {
            //перенес из onPrepareOptionsMenu потому что onPrepareOptionsMenu вызывается каждый
            //раз когда на "три точки" нажимаешь
            menu.setGroupVisible(R.id.additional_group, true);
            labelNameTv = menu.findItem(R.id.add_label).getActionView().findViewById(R.id.label_name);
            labelNameTv.setText(chosenLabel.getTitle());
            View view = menu.findItem(R.id.add_label).getActionView();
            view.findViewById(R.id.add_label_main_container).setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    currentFragmentModeSubject.onNext(PhotoEditorFragment.FragmentMode.CHOOSE_LABEL);
                }
            });
        }
    }

    public void onPrepareOptionsMenu(Menu menu) {

    }

    public void onStop() {
        super.onStop();
        disposables.clear();
    }

    public void onDestroy() {
        super.onDestroy();
        if (originalSoftMode != null)
            getActivity().getWindow().setSoftInputMode(originalSoftMode);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (addCaptionMode != null) {
            addCaptionMode.dispose();
        }
        //если этого не сделать то происходит утечка памяти
        //??
        addTextMode = null;
        addCaptionMode = null;
        painterMode = null;
        chooseLabelMode = null;

        view = null;
        mLayout = null;
//        colorPickerView = null;
//        colorPicker = null;
//        doneAreaRl = null;
//        doneFabProgress = null;
//        donePaintBtn = null;
//        cancelPaintBtn = null;
//        relativeLayoutPopWindow = null;
//        chosenLabel = null;
//        labelNameTv = null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            if (getActivity() instanceof OnBackClickListener) {
                ((OnBackClickListener) getActivity()).onBackClick();
            }
            return true;
        } else if (itemId == R.id.brush_item_menu) {
            currentFragmentModeSubject.onNext(PhotoEditorFragment.FragmentMode.PAINTER);
            return true;
        } else if (itemId == R.id.text_item_menu) {
            editTextChangeString = "";
            currentFragmentModeSubject.onNext(PhotoEditorFragment.FragmentMode.ADD_TEXT);
            return true;
        } else if (itemId == R.id.add_label) {
            currentFragmentModeSubject.onNext(PhotoEditorFragment.FragmentMode.CHOOSE_LABEL);
        } else if (itemId == R.id.share_item_menu) {
            shareImage();
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onEditTextChangeListener(String text, int colorCode) {
        editTextChangeString = text;
        editTextChangeColorCode = colorCode;
        currentFragmentModeSubject.onNext(PhotoEditorFragment.FragmentMode.ADD_TEXT);
    }

    @Override
    public void onAddViewListener(ViewType viewType, int i) {

    }

    @Override
    public void onRemoveViewListener(int i) {

    }

    @Override
    public void onStartViewChangeListener(ViewType viewType) {
    }

    @Override
    public void onStopViewChangeListener(ViewType viewType) {
    }

    private void initColorPicker() {
        disposables.add(colorPicker.getBrush().subscribe(new Consumer<ColorPicker.Brush>() {
            public void accept(ColorPicker.Brush brush) throws Exception {
                photoEditorSDK.setBrushColor(brush.getColor());
                photoEditorSDK.setBrushSize(brush.getSize());
            }
        }));
    }

    private void initDefaultColors() {
        int color = colorPicker.getSwatch().color;
        float brushSize = colorPicker.getSwatch().brushWeight * 100.0F;
        photoEditorSDK.setBrushColor(color);
        photoEditorSDK.setBrushSize(brushSize);
    }

    private Label checkLabel() {
        if (chosenLabel != null && chosenLabel.isCorrectly()) {
            return chosenLabel;
        }
        return chosenLabel;
    }

    private String checkCaption() {
        if (addCaptionMode != null && !addCaptionMode.getCaption().isEmpty()) {
            return addCaptionMode.getCaption();
        }
        return null;
    }

    private void setClickListenersForFabButton() {
        doneAreaRl.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        doneAreaRl.setEnabled(false);
                        if (isRequiredCaption && !(addCaptionMode != null && StringUtils.stringIsNotEmpty(addCaptionMode.getCaption()))) {
                            new AlertDialog.Builder(getContext())
                                    .setTitle(R.string.gs_photoeditor_lib_add_a_caption_if_required_alert_dialog_title)
                                    .setMessage(R.string.gs_photoeditor_lib_add_a_caption_if_required_alert_dialog_message)
                                    .setNegativeButton(getString(R.string.gs_photoeditor_lib_ok), new android.content.DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                            doneAreaRl.setEnabled(true);
                                        }
                                    }).create().show();
                            return;
                        }

                        if (outputPath != null) {
                            doneFabProgress.setVisibility(View.VISIBLE);

                            Disposable disposable = Observable.fromCallable(
                                    () -> {
                                        Log.d("Done btn", String.valueOf(Thread.currentThread().getId()));
                                        processingAndSaveImage();
                                        return outputPath;
                                    })
                                    .compose(new ApplySchedulers<>())
                                    .subscribe(
                                            uri -> {
                                                Activity activity = getActivity();
                                                if (activity != null) {
                                                    //как-то не красиво
                                                    Label tempLabel = checkLabel();
                                                    String caption = checkCaption();
                                                    if (activity instanceof PhotoEditorFinishingCallback) {
                                                        ((PhotoEditorFinishingCallback) activity).onSuccess(tempLabel, caption);
                                                    } else {
                                                        //fixme наверное фрагмент из билиотеки не должен этим pfybvfnmcz
                                                        //fixme убрать это и бросать исключение если !(instanceof PhotoEditorFinishingCallback)
                                                        Intent intent = new Intent();
                                                        intent.setData(uri);
                                                        if (tempLabel != null) {
                                                            intent.putExtra(INTENT_TAG_CHOSEN_LABEL, tempLabel);
                                                        }
                                                        if (caption != null) {
                                                            intent.putExtra(INTENT_TAG_CAPTION, caption);
                                                        }
                                                        activity.setResult(RESULT_OK, intent);
                                                        activity.finish();
                                                    }
                                                }

                                            },
                                            throwable -> {
                                                doneFabProgress.setVisibility(View.GONE);
                                                doneAreaRl.setEnabled(true);
//                                                Toast.makeText(getContext(), "Error: " + throwable.getMessage(), Toast.LENGTH_LONG).show();
                                                Activity activity = getActivity();
                                                if (activity instanceof PhotoEditorFinishingCallback) {
                                                    ((PhotoEditorFinishingCallback) activity).onFailure(throwable);
                                                }
                                            });
                            disposables.add(disposable);
                        } else {
                            doneAreaRl.setEnabled(true);
                            (new AlertDialog.Builder(getContext()))
                                    .setTitle(R.string.gs_photoeditor_lib_error_by_save)
                                    .setMessage(String.format(getString(R.string.gs_photoeditor_lib_picture_path), outputPath))
                                    .setCancelable(false)
                                    .setNegativeButton(getString(R.string.gs_photoeditor_lib_ok), new android.content.DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    }).create().show();
                        }

                    }
                });
    }

    private void processingAndSaveImage() throws IOException {
        Bitmap bitmap = photoEditorSDK.getBitmap();
        if (isNeedStamp()) BitmapUtils.setDateToBitmap(bitmap, date);
        BitmapUtils.saveBitmap(bitmap, quality, outputPath.getPath());
        if (addCaptionMode != null)
            BitmapUtils.setExifToPhoto(addCaptionMode.getCaption(), outputPath.getPath());
    }

    private View.OnKeyListener getOnKeyListenerForAddTextMode() {
        return new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() != 0) {
                    return false;
                } else {
                    if (event.getAction() == 0 && keyCode == 4 && currentFragmentModeSubject.getValue() != PhotoEditorFragment.FragmentMode.DEFAULT) {
                        if (pop != null) {
                            pop.dismiss();
                        }

                        currentFragmentModeSubject.onNext(PhotoEditorFragment.FragmentMode.DEFAULT);
                    }

                    return true;
                }
            }
        };
    }

    private void setVisibilitySupportActionBar(boolean isVisibility) {
        Activity activity = getActivity();
        if (activity != null) {
            activity.closeOptionsMenu();
            ActionBar actionBar = ((AppCompatActivity) activity).getSupportActionBar();
            if (actionBar != null) {
                if (isVisibility) {
                    actionBar.show();
                } else {
                    actionBar.hide();
                }
            }
        }
    }

    @Deprecated
    private void shareImage() {
        try {
            processingAndSaveImage();
            final Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("image/jpg");
            shareIntent.putExtra(Intent.EXTRA_STREAM, outputPath);
            startActivity(Intent.createChooser(shareIntent, getString(R.string.gs_photoeditor_lib_share_string)));
            if (shareActionProvider != null) {
                shareActionProvider.setShareIntent(shareIntent);
            }
        } catch (Exception ex) {
            Toast.makeText(getContext(), R.string.gs_photoeditor_lib_sharing_image_error, Toast.LENGTH_LONG).show();
        }
    }

    public enum FragmentMode {
        DEFAULT,
        PAINTER,
        ADD_TEXT,
        CHOOSE_LABEL,
        ADD_CAPTION;

        public static FragmentMode valueOf(int ordinal) {
            return values()[ordinal];
        }
    }

    private class ChangeMode {
        private BehaviorSubject<List<Transition>> transitionObservable;
        private Consumer<PhotoEditorFragment.FragmentMode> consumer;
        private Consumer<Integer> transitionsConsumer;

        private ChangeMode() {
            transitionObservable = BehaviorSubject.create();
            consumer = new Consumer<PhotoEditorFragment.FragmentMode>() {
                private void setTransitions(boolean isScreenOn) {
                    ArrayList<Transition> transitions = new ArrayList();
                    transitions.add(AnimateUtils.createTransitionToRight(doneAreaRl, isScreenOn));
                    transitionObservable.onNext(new ArrayList(transitions));
                }

                public void accept(PhotoEditorFragment.FragmentMode fragmentMode) throws Exception {
                    if (fragmentMode != PhotoEditorFragment.FragmentMode.DEFAULT) {
                        setTransitions(false);
                    } else {
                        setTransitions(true);
                    }

                }
            };
            transitionsConsumer = new Consumer<Integer>() {
                public void accept(Integer o) throws Exception {
                    if (currentFragmentModeSubject.getValue() == FragmentMode.ADD_CAPTION) return;
                    if (currentFragmentModeSubject.getValue() != PhotoEditorFragment.FragmentMode.DEFAULT) {
                        doneAreaRl.setVisibility(View.GONE);
                        setVisibilitySupportActionBar(false);
                    } else {
                        doneAreaRl.setVisibility(View.VISIBLE);
                        setVisibilitySupportActionBar(true);
                    }

                }
            };
        }

        Consumer<Integer> getTransitionsConsumer() {
            return transitionsConsumer;
        }

        BehaviorSubject<List<Transition>> getTransitionObservable() {
            return transitionObservable;
        }

        Consumer<PhotoEditorFragment.FragmentMode> getConsumer() {
            return consumer;
        }
    }

    private class PainterMode {
        private BehaviorSubject<List<Transition>> transitionObservable;
        private Consumer<PhotoEditorFragment.FragmentMode> consumer;
        private Consumer<Integer> transitionsConsumer;

        private PainterMode() {
            transitionObservable = BehaviorSubject.create();
            consumer = new Consumer<PhotoEditorFragment.FragmentMode>() {
                {
                    initOnClicks();
                }

                private void setTransitions(boolean isScreenOn) {
                    ArrayList<Transition> transitions = new ArrayList();
                    transitions.add(AnimateUtils.createTransitionToBottom(colorPickerView, isScreenOn));
                    transitionObservable.onNext(transitions);
                }

                private void initOnClicks() {
                    donePaintBtn.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            currentFragmentModeSubject.onNext(PhotoEditorFragment.FragmentMode.DEFAULT);
                        }
                    });
                    cancelPaintBtn.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            photoEditorSDK.clearBrushAllViews();
                        }
                    });
                }

                public void accept(PhotoEditorFragment.FragmentMode fragmentMode) throws Exception {
                    if (fragmentMode == PhotoEditorFragment.FragmentMode.PAINTER) {
                        photoEditorSDK.setBrushDrawingMode(true);
                        setTransitions(true);
                    } else if (colorPickerView.getVisibility() == View.VISIBLE) {
                        photoEditorSDK.setBrushDrawingMode(false);
                        setTransitions(false);
                    } else {
                        transitionObservable.onNext(new ArrayList());
                    }

                }
            };
            transitionsConsumer = new Consumer<Integer>() {
                public void accept(Integer o) throws Exception {
                    Log.d("PainterMode", "transitionsConsumer.accept");
                    if (currentFragmentModeSubject.getValue() == PhotoEditorFragment.FragmentMode.PAINTER) {
                        colorPickerView.setVisibility(View.VISIBLE);
                    } else if (colorPickerView.getVisibility() == View.VISIBLE) {
                        colorPickerView.setVisibility(View.GONE);
                    }
                }
            };
        }

        Consumer<Integer> getTransitionsConsumer() {
            return transitionsConsumer;
        }

        BehaviorSubject<List<Transition>> getTransitionObservable() {
            return transitionObservable;
        }

        Consumer<PhotoEditorFragment.FragmentMode> getConsumer() {
            return consumer;
        }
    }

    private class AddTextMode {
        private BehaviorSubject<List<Transition>> transitionObservable;
        private View addTextPopupWindowRootView;
        private ColorPicker addTextColorPicker;
        private EditText addTextEditText;
        private Button cancelBtn;
        private Button doneBtn;
        private Disposable colorsSubscription;
        private Consumer<PhotoEditorFragment.FragmentMode> consumer;
        private Consumer<Integer> transitionsConsumer;

        private AddTextMode() {
            transitionObservable = BehaviorSubject.create();
            consumer = new Consumer<PhotoEditorFragment.FragmentMode>() {
                public void accept(PhotoEditorFragment.FragmentMode fragmentMode) throws Exception {
                    if (fragmentMode == PhotoEditorFragment.FragmentMode.ADD_TEXT) {
                        if (getActivity() == null) {
                            return;
                        }

                        if (addTextPopupWindowRootView == null) {
                            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            addTextPopupWindowRootView = inflater.inflate(R.layout.gs_photoeditor_lib_add_text_popup_window, (ViewGroup) null);
                            relativeLayoutPopWindow = (RelativeLayout) addTextPopupWindowRootView.findViewById(R.id.popup_window_rl);
                            addTextColorPicker = (ColorPicker) addTextPopupWindowRootView.findViewById(R.id.drawing_view_color_picker);
                            addTextColorPicker.setLocation(1.0F);
                            addTextEditText = (EditText) addTextPopupWindowRootView.findViewById(R.id.add_text_edit_text);
                            cancelBtn = (Button) addTextPopupWindowRootView.findViewById(R.id.cancel_btn);
                            doneBtn = (Button) addTextPopupWindowRootView.findViewById(R.id.done_btn);
                            pop = new PopupWindow(getContext());
                            pop.setContentView(addTextPopupWindowRootView);
                            pop.setWidth(-1);
                            pop.setHeight(-1);
                            pop.setBackgroundDrawable((Drawable) null);
                            pop.getContentView().setFocusableInTouchMode(true);
                            pop.getContentView().setOnKeyListener(getOnKeyListenerForAddTextMode());

                            addTextEditText.setOnKeyListener(getOnKeyListenerForAddTextMode());
                            addTextEditText.addTextChangedListener(new TextWatcher() {
                                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                                }

                                public void onTextChanged(CharSequence s, int start, int before, int count) {
                                }

                                public void afterTextChanged(Editable s) {
                                    editTextChangeString = String.valueOf(s);
                                }
                            });
                            cancelBtn.setText(R.string.gs_photoeditor_lib_btn_cancel);
                            cancelBtn.setOnClickListener(new View.OnClickListener() {
                                public void onClick(View v) {
                                    AnimateUtils.createTransitionEnteringScreenOff(relativeLayoutPopWindow, new Transition[]{AnimateUtils.createTransitionToBottom(addTextColorPicker, false), AnimateUtils.createTransitionToBottom(pop.getContentView(), false)});
                                    currentFragmentModeSubject.onNext(PhotoEditorFragment.FragmentMode.DEFAULT);
                                    pop.dismiss();
                                }
                            });
                            doneBtn.setOnClickListener(new View.OnClickListener() {
                                public void onClick(View v) {
                                    if (StringUtils.stringIsNotEmpty(addTextEditText.getText().toString())) {
                                        photoEditorSDK.addText(addTextEditText.getText().toString(), editTextChangeColorCode);
                                    }

                                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                    if (imm != null) {
                                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                                    }

                                    pop.dismiss();
                                    transitionObservable.onNext(new ArrayList());
                                    currentFragmentModeSubject.onNext(PhotoEditorFragment.FragmentMode.DEFAULT);
                                }
                            });
                        }

                        if (editTextChangeColorCode == -1) {
                            addTextColorPicker.setLocation(1.0F);
                        }

                        addTextEditText.requestFocus();
                        addTextEditText.setText(editTextChangeString);
                        addTextEditText.setTextColor(editTextChangeColorCode);
                        if (colorsSubscription == null || colorsSubscription.isDisposed()) {
                            colorsSubscription = addTextColorPicker.getBrush().subscribe(new Consumer<ColorPicker.Brush>() {
                                public void accept(ColorPicker.Brush brush) throws Exception {
                                    addTextEditText.setTextColor(brush.getColor());
                                    editTextChangeColorCode = brush.getColor();
                                }
                            });
                        }

                        disposables.add(colorsSubscription);
                        if (!pop.isShowing()) {
                            pop.setFocusable(true);
                            pop.setSoftInputMode(16);
                            pop.setInputMethodMode(1);
                            pop.showAtLocation(addTextPopupWindowRootView, 48, 0, 0);
                            if (getActivity() != null) {
//                                getActivity().getWindow().setSoftInputMode(16);
                                addTextEditText.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                        imm.showSoftInput(addTextEditText, 0);
                                    }
                                }, 50);
//                                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//                                if (imm != null) {
//                                    imm.toggleSoftInput(2, 0);
//                                }
                            }
                        }

                        transitionObservable.onNext(new ArrayList());
                    } else {
                        if (pop != null) {
                            pop.dismiss();
                        }

                        transitionObservable.onNext(new ArrayList());
                    }

                }
            };
            transitionsConsumer = new Consumer<Integer>() {
                public void accept(Integer o) throws Exception {
                    Log.d("AddTextMode", "transitionsConsumer.accept");
                }
            };
        }

        Consumer<Integer> getTransitionsConsumer() {
            return transitionsConsumer;
        }

        BehaviorSubject<List<Transition>> getTransitionObservable() {
            return transitionObservable;
        }

        Consumer<PhotoEditorFragment.FragmentMode> getConsumer() {
            return consumer;
        }
    }

    private class ChooseLabelMode {
        private AlertDialog dialog;
        private BehaviorSubject<List<Transition>> transitionObservable;
        private Consumer<Integer> transitionsConsumer;
        private Consumer<PhotoEditorFragment.FragmentMode> consumer;

        private ChooseLabelMode() {
            transitionObservable = BehaviorSubject.create();
            transitionsConsumer = new Consumer<Integer>() {
                public void accept(Integer o) throws Exception {
                }
            };
            consumer = new Consumer<PhotoEditorFragment.FragmentMode>() {
                ImageView labelIconIv;

                public void accept(PhotoEditorFragment.FragmentMode fragmentMode) throws Exception {
                    if (fragmentMode == PhotoEditorFragment.FragmentMode.CHOOSE_LABEL) {
                        if (dialog == null) {
                            String[] labelsTitles = new String[labels.size()];

                            for (int i = 0; i < labels.size(); ++i) {
                                labelsTitles[i] = ((Label) labels.get(i)).getTitle();
                            }

                            dialog = (new AlertDialog.Builder(getContext()))
                                    .setTitle(getString(R.string.gs_photoeditor_lib_choose_stiker))
                                    .setCancelable(false)
                                    .setItems(labelsTitles, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            Label label = (Label) labels.get(which);
                                            setChosenLabel(label);
                                            labelNameTv.setText(label.getTitle());
                                            currentFragmentModeSubject.onNext(FragmentMode.DEFAULT);
                                        }
                                    }).create();//.show();
                            dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                                @Override
                                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                                    if (event.getAction() != 0) {
                                        return false;
                                    } else {
                                        if (event.getAction() == 0 && keyCode == 4 && currentFragmentModeSubject.getValue() != PhotoEditorFragment.FragmentMode.DEFAULT) {
                                            if (dialog != null) {
                                                dialog.dismiss();
                                            }
                                            currentFragmentModeSubject.onNext(PhotoEditorFragment.FragmentMode.DEFAULT);
                                        }
                                        return true;
                                    }
                                }
                            });
                            dialog.show();
                        } else {
                            dialog.show();
                        }
                    } else {
                        if (dialog != null && dialog.isShowing())
                            dialog.dismiss();
                    }

                }
            };
        }

        Consumer<Integer> getTransitionsConsumer() {
            return transitionsConsumer;
        }

        BehaviorSubject<List<Transition>> getTransitionObservable() {
            return transitionObservable;
        }

        Consumer<PhotoEditorFragment.FragmentMode> getConsumer() {
            return consumer;
        }
    }

    private class AddCaptionMode implements KeyboardHeightProvider.KeyboardHeightObserver {
        private BehaviorSubject<List<Transition>> transitionObservable;
        private Consumer<PhotoEditorFragment.FragmentMode> consumer;
        private Consumer<Integer> transitionsConsumer;
        private View addCaptionParent;
        private EditText addCaptionEditText;
        private View addCaptionDoneBtn;

        private AddCaptionMode() {
            transitionObservable = BehaviorSubject.create();
            transitionsConsumer = new Consumer<Integer>() {
                public void accept(Integer o) throws Exception {
                    Log.d("AddCaptionMode", "transitionsConsumer.accept");
                    if (currentFragmentModeSubject.getValue() == FragmentMode.DEFAULT) {
                        addCaptionParent.setVisibility(View.VISIBLE);
//                        addCaptionDoneBtn.setVisibility(View.INVISIBLE);
                    } else if (currentFragmentModeSubject.getValue() != FragmentMode.ADD_CAPTION) {
                        addCaptionParent.setVisibility(View.GONE);
                    } else {
                        addCaptionParent.setVisibility(View.VISIBLE);
                        addCaptionDoneBtn.setVisibility(View.VISIBLE);
                    }
                }
            };

            keyboardHeightProvider = new KeyboardHeightProvider(getActivity());
            keyboardHeightProvider.setKeyboardHeightObserver(this);
            mLayout.post(new Runnable() {
                @Override
                public void run() {
                    keyboardHeightProvider.start();
                }
            });
            ViewStub viewStub = mLayout.findViewById(R.id.add_text_vs);
            addCaptionParent = viewStub.inflate();
            addCaptionEditText = addCaptionParent.findViewById(R.id.add_caption_et);
            addCaptionDoneBtn = addCaptionParent.findViewById(R.id.add_caption_button_ok);
            if (isRequiredCaption) {
                addCaptionEditText.setHint(R.string.gs_photoeditor_lib_add_a_caption_if_required);
            }

            addCaptionDoneBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    hideKeyobard();
                }
            });

            addCaptionEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus)
                        currentFragmentModeSubject.onNext(FragmentMode.ADD_CAPTION);
                }
            });

            consumer = new Consumer<FragmentMode>() {
                private void setTransitions(boolean isScreenOn) {
                    ArrayList<Transition> transitions = new ArrayList();
//                    transitions.add(AnimateUtils.createTransitionToBottom(addCaptionParent, isScreenOn));
                    transitionObservable.onNext(transitions);
                }

                @Override
                public void accept(FragmentMode fragmentMode) throws Exception {
                    if (fragmentMode == FragmentMode.DEFAULT) {
                        setTransitions(true);
                        viewsToDefault();
                    } else if (fragmentMode != FragmentMode.ADD_CAPTION) {
                        setTransitions(false);
                        viewsToDefault();
                    } else {
                        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING | WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
//                        viewsActived();
                    }
                }
            };
        }

        public void dispose() {
            keyboardHeightProvider.setKeyboardHeightObserver(null);
            keyboardHeightProvider.close();
        }

        public Consumer<Integer> getTransitionsConsumer() {
            return transitionsConsumer;
        }

        public BehaviorSubject<List<Transition>> getTransitionObservable() {
            return transitionObservable;
        }

        public Consumer<FragmentMode> getConsumer() {
            return consumer;
        }

        private void viewsActived() {
//            addCaptionParent.setVisibility(View.VISIBLE);
//            addCaptionEditText.setVisibility(View.VISIBLE);
            addCaptionDoneBtn.setVisibility(View.VISIBLE);
        }

        private void viewsToDefault() {
            hideKeyobard();
            editTextCustomMargin(0);
            addCaptionEditText.clearFocus();
        }

        private void hideKeyobard() {
            InputMethodManager imm = (InputMethodManager) PhotoEditorFragment.this.getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(addCaptionParent.getWindowToken(), 0);
        }

        private void editTextCustomMargin(int margin) {
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) addCaptionParent.getLayoutParams();
            params.setMargins(0, 0, 0, margin);
            addCaptionParent.setLayoutParams(params);
            addCaptionParent.requestLayout();
        }

        @Override
        public void onKeyboardHeightChanged(int height, int orientation) {
            if (currentFragmentModeSubject.getValue() == FragmentMode.ADD_CAPTION) {
                editTextCustomMargin(height);
                if (height == 0) {// keyboard closed
                    currentFragmentModeSubject.onNext(FragmentMode.DEFAULT);
                }
            }
        }

        public String getCaption() {
            return addCaptionEditText.getText().toString();
        }

        public void setCaption(String caption) {
            addCaptionEditText.setText(caption);
        }
    }
}
