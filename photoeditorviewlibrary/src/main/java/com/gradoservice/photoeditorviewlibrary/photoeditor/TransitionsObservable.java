package com.gradoservice.photoeditorviewlibrary.photoeditor;

import androidx.transition.Transition;
import androidx.transition.TransitionSet;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;

public class TransitionsObservable {
    public TransitionsObservable() {
    }

    public static Observable add(Observable... observables) {
//        ArrayList<ObservableSource<?>> list = new ArrayList<>();
//        for (Observable observable : observables) {
//            if (observable != null) {
//                list.add(observable);
//            }
//        }

        return Observable.zip(Arrays.<ObservableSource<? extends ObservableSource<?>>>asList(observables), new Function<Object[], TransitionSet>() {
            public TransitionSet apply(Object[] objects) throws Exception {
                TransitionSet set = new TransitionSet();
                set.setOrdering(0);
                set.setDuration(225L);
                Object[] var3 = objects;
                int var4 = objects.length;

                for (int var5 = 0; var5 < var4; ++var5) {
                    Object object = var3[var5];
                    if (object instanceof List) {
                        Iterator var7 = ((List) object).iterator();

                        while (var7.hasNext()) {
                            Object transition = var7.next();
                            if (transition instanceof Transition) {
                                set.addTransition((Transition) transition);
                            }
                        }
                    }
                }

                objects = null;
                return set;
            }
        });
    }
}
