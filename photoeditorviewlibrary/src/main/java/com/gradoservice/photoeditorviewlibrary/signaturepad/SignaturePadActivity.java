package com.gradoservice.photoeditorviewlibrary.signaturepad;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import com.gradoservice.photoeditorviewlibrary.R;

public class SignaturePadActivity extends AppCompatActivity {
    public static String SIGNATURE_PAD_THEME_KEY = "signature_pad_theme_key";
    public static String SIGNATURE_PAD_IS_HIDE_ACTION_BAR = "signature_pad_theme_key";
    private int theme;
    private boolean hideBar;

    public SignaturePadActivity() {
    }

    public static Intent getIntent(Context context, Bundle bundle) {
        Intent intent = new Intent(context, SignaturePadActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }

        return intent;
    }

    protected void onCreate(Bundle savedInstanceState) {
        Bundle bundle = this.getIntent().getExtras();
        if (bundle != null) {
            this.theme = bundle.getInt(SIGNATURE_PAD_THEME_KEY);
            if (this.theme != 0) {
                this.setTheme(this.theme);
            }

            this.hideBar = bundle.getBoolean(SIGNATURE_PAD_IS_HIDE_ACTION_BAR);
        }

        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.gs_photoeditor_lib_activity_signature_pad);
        ActionBar actionBar = this.getActionBar();
        if (actionBar != null && this.hideBar) {
            actionBar.hide();
        }

        this.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new SignaturePadFragment()).commit();
    }
}