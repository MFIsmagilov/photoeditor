package com.gradoservice.photoeditorviewlibrary.signaturepad;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.transition.Transition;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.ahmedadeltito.photoeditorsdk.BrushDrawingView;
import com.ahmedadeltito.photoeditorsdk.OnPhotoEditorSDKListener;
import com.ahmedadeltito.photoeditorsdk.PhotoEditorSDK;
import com.ahmedadeltito.photoeditorsdk.ViewType;
import com.gradoservice.photoeditorviewlibrary.R;
import com.gradoservice.photoeditorviewlibrary.common.fragment.BaseEditorFragment;
import com.gradoservice.photoeditorviewlibrary.common.widget.ColorPicker;
import com.gradoservice.photoeditorviewlibrary.utils.AnimateUtils;

import java.io.File;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

public class SignaturePadFragment extends BaseEditorFragment implements OnPhotoEditorSDKListener {
    public static String TAG = SignaturePadFragment.class.getSimpleName();
    private RelativeLayout mLayout;
    private RelativeLayout parentImageRelativeLayout;
    private RelativeLayout relativeLayoutPopWindow;
    private FloatingActionButton deleteView;
    private ImageView photoEditImageView;
    private BrushDrawingView brushDrawingView;
    private Menu menu;
    private RelativeLayout colorPickerView;
    private ColorPicker colorPicker;
    private FloatingActionButton recyclerBinFabBtn;
    private FloatingActionButton doneFabBtn;
    private Button donePaintBtn;
    private Button cancelPaintBtn;
    private boolean isSignatureChanged = false;
    private File savedSignature = null;
    private FloatingActionButton settingFabBtn;
    private CompositeDisposable disposables = new CompositeDisposable();

    public SignaturePadFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.gs_photoeditor_lib_new_ed, container, false);
        Bitmap bitmap = this.createWhiteBitmap();
        this.mLayout = (RelativeLayout) view.findViewById(R.id.parent);
        this.parentImageRelativeLayout = (RelativeLayout) view.findViewById(R.id.parent_image_rl);
        this.photoEditImageView = (ImageView) view.findViewById(R.id.photo_edit_iv);
        this.brushDrawingView = (BrushDrawingView) view.findViewById(R.id.drawing_view);
        this.colorPickerView = (RelativeLayout) view.findViewById(R.id.drawing_view_pickers_view);
        this.photoEditImageView.setImageBitmap(bitmap);
        this.colorPicker = (ColorPicker) view.findViewById(R.id.drawing_view_color_picker);
        this.recyclerBinFabBtn = (FloatingActionButton) view.findViewById(R.id.fab_recyclerbin);
        this.doneFabBtn = (FloatingActionButton) view.findViewById(R.id.fab_done);
        this.settingFabBtn = (FloatingActionButton) view.findViewById(R.id.fab_camera);
        view.findViewById(R.id.fab_camera).setVisibility(View.GONE);
        view.findViewById(R.id.fab_recyclerbin).setVisibility(View.GONE);
        this.donePaintBtn = (Button) view.findViewById(R.id.done_btn);
        this.cancelPaintBtn = (Button) view.findViewById(R.id.cancel_btn);
        this.donePaintBtn.setTextColor(ContextCompat.getColor(this.getActivity(), R.color.colorPrimary));
        this.cancelPaintBtn.setTextColor(ContextCompat.getColor(this.getActivity(), R.color.colorPrimary));
        this.donePaintBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AnimateUtils.createTransitionEnteringScreenOn(SignaturePadFragment.this.mLayout, new Transition[]{AnimateUtils.createTransitionToLeft(SignaturePadFragment.this.settingFabBtn, true), AnimateUtils.createTransitionToRight(SignaturePadFragment.this.doneFabBtn, true), AnimateUtils.createTransitionToBottom(SignaturePadFragment.this.colorPickerView, false)});
                SignaturePadFragment.this.doneFabBtn.show();
                SignaturePadFragment.this.settingFabBtn.show();
                SignaturePadFragment.this.colorPickerView.setVisibility(View.GONE);
            }
        });
        this.cancelPaintBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SignaturePadFragment.this.photoEditorSDK.clearBrushAllViews();
                SignaturePadFragment.this.isSignatureChanged = false;
            }
        });
        this.photoEditorSDK = (new PhotoEditorSDK.PhotoEditorSDKBuilder(this.getActivity())).parentView(this.parentImageRelativeLayout).childView(this.photoEditImageView).brushDrawingView(this.brushDrawingView).buildPhotoEditorSDK();
        this.photoEditorSDK.setOnPhotoEditorSDKListener(this);
        this.photoEditorSDK.setBrushDrawingMode(true);
        this.initColorPicker();
        this.doneFabBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (SignaturePadFragment.this.isSignatureChanged) {
                    SignaturePadFragment.this.savedSignature = SignaturePadFragment.this.saveSignatureToGallery();
                    SignaturePadFragment.this.positiveClose();
                } else {
                    Snackbar.make(SignaturePadFragment.this.getView(), SignaturePadFragment.this.getString(R.string.gs_photoeditor_lib_gs_photoeditor_lib_signature_not_found), Snackbar.LENGTH_LONG).show();
                }

            }
        });
        this.settingFabBtn.setImageResource(R.drawable.gs_photoeditor_lib_ic_settings);
        this.settingFabBtn.show();
        this.settingFabBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AnimateUtils.createTransitionEnteringScreenOn(SignaturePadFragment.this.mLayout, new Transition[]{AnimateUtils.createTransitionToLeft(SignaturePadFragment.this.settingFabBtn, false), AnimateUtils.createTransitionToRight(SignaturePadFragment.this.doneFabBtn, false), AnimateUtils.createTransitionToBottom(SignaturePadFragment.this.colorPickerView, true)});
                SignaturePadFragment.this.doneFabBtn.hide();
                SignaturePadFragment.this.settingFabBtn.hide();
                SignaturePadFragment.this.colorPickerView.setVisibility(View.VISIBLE);
            }
        });
        return view;
    }

    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                Log.d("KEY", keyCode + " " + 4);
                if (event.getAction() != 0) {
                    return false;
                } else {
                    if (event.getAction() == 0 && keyCode == 4) {
                        SignaturePadFragment.this.closeActivity();
                    }

                    return true;
                }
            }
        });
    }

    private void initColorPicker() {
        disposables.add(this.colorPicker.getBrush().subscribe(new Consumer<ColorPicker.Brush>() {
            public void accept(ColorPicker.Brush brush) throws Exception {
                SignaturePadFragment.this.photoEditorSDK.setBrushColor(brush.getColor());
                SignaturePadFragment.this.photoEditorSDK.setBrushSize(brush.getSize());
            }
        }));
    }

    public void onEditTextChangeListener(String text, int colorCode) {
        throw new UnsupportedOperationException();
    }

    public void onAddViewListener(ViewType viewType, int numberOfAddedViews) {
        Log.d("SPF", "onAddViewListener");
    }

    public void onRemoveViewListener(int numberOfAddedViews) {
        Log.d("SPF", "onRemoveViewListener");
    }

    public void onStartViewChangeListener(ViewType viewType) {
        Log.d("SPF", "onStartViewChangeListener");
        this.isSignatureChanged = true;
    }

    public void onStopViewChangeListener(ViewType viewType) {
        Log.d("SPF", "onStopViewChangeListener");
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.signature_pad_settings) {
            this.doneFabBtn.hide();
            this.colorPickerView.setVisibility(View.VISIBLE);
            ((AppCompatActivity) this.getActivity()).getSupportActionBar().hide();
            return true;
        } else {
            if (itemId == 16908332) {
                this.closeActivity();
            }

            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        disposables.clear();
    }

    private Bitmap createWhiteBitmap() {
        DisplayMetrics metrics = new DisplayMetrics();
        this.getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        Bitmap bitmap = Bitmap.createBitmap(metrics.widthPixels, metrics.heightPixels, Bitmap.Config.ARGB_8888);
        bitmap.eraseColor(-1);
        return bitmap;
    }

    private File saveSignatureToGallery() {
        try {
            String path = this.photoEditorSDK.saveImage("MapMobile", String.format("mm_signature_%d.jpg", System.currentTimeMillis()));
            Toast.makeText(this.getActivity(), this.getString(R.string.gs_photoeditor_lib_gs_photoeditor_lib_add_signature_saved), Toast.LENGTH_LONG).show();
            return new File(path);
        } catch (Exception var3) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity(), R.style.Theme_AppCompat_Light_Dialog_Alert);
            builder.setTitle(R.string.gs_photoeditor_lib_add_signature_error).setMessage(var3.getMessage()).setPositiveButton(this.getString(R.string.gs_photoeditor_lib_ok), new android.content.DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).create().show();
            var3.printStackTrace();
            return null;
        }
    }

    private void closeActivity() {
        if (!this.isSignatureChanged) {
            this.getActivity().onBackPressed();
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity(), R.style.Theme_AppCompat_Light_Dialog_Alert);
            builder.setTitle(R.string.gs_photoeditor_lib_add_signature_attention).setMessage(R.string.gs_photoeditor_lib_add_signature_save_warning).setPositiveButton(this.getString(R.string.gs_photoeditor_lib_add_signature_save_dialog_ok), new android.content.DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    SignaturePadFragment.this.negativeClose();
                }
            }).setNegativeButton(this.getString(R.string.gs_photoeditor_lib_add_signature_save_dialog_cancel), new android.content.DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    SignaturePadFragment.this.savedSignature = SignaturePadFragment.this.saveSignatureToGallery();
                    SignaturePadFragment.this.positiveClose();
                }
            }).create().show();
        }

    }

    private void positiveClose() {
        Intent intent = new Intent();
        intent.setData(Uri.fromFile(this.savedSignature));
        this.getActivity().setResult(-1, intent);
        this.getActivity().finish();
    }

    private void negativeClose() {
        this.getActivity().setResult(0);
        this.getActivity().finish();
    }
}