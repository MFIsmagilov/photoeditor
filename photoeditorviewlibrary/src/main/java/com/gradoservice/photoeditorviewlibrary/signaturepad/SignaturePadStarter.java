package com.gradoservice.photoeditorviewlibrary.signaturepad;

import android.app.Activity;
import android.os.Bundle;
import android.util.DisplayMetrics;

import com.gradoservice.photoeditorviewlibrary.common.widget.AndroidUtilities;

/**
 * Created by maratismagilov on 20.03.2018.
 */
public class SignaturePadStarter {
    private Bundle bundle = new Bundle(1);
    private Activity context;

    public SignaturePadStarter(Activity context) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        AndroidUtilities.setDensity(metrics.density);
        this.context = context;
    }

    public SignaturePadStarter setTheme(int theme) {
        this.bundle.putInt(SignaturePadActivity.SIGNATURE_PAD_THEME_KEY, theme);
        return this;
    }

    public SignaturePadStarter hideActionBar(boolean isHide) {
        this.bundle.putBoolean(SignaturePadActivity.SIGNATURE_PAD_IS_HIDE_ACTION_BAR, isHide);
        return this;
    }

    public void startActivityForResult(int requestCode) {
        this.context.startActivityForResult(SignaturePadActivity.getIntent(this.context, this.bundle), requestCode);
    }
}
