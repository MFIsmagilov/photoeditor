package com.gradoservice.photoeditorviewlibrary.utils;

import android.animation.TimeInterpolator;
import androidx.annotation.NonNull;
import androidx.transition.Slide;
import androidx.transition.Transition;
import androidx.transition.TransitionManager;
import androidx.transition.TransitionSet;
import androidx.interpolator.view.animation.FastOutLinearInInterpolator;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;

/**
 * Created by maratismagilov on 23.03.2018.
 */

public class AnimateUtils {
    public AnimateUtils() {
    }

    public static void createTransitionEnteringScreenOn(@NonNull ViewGroup viewGroup, Transition... transitions) {
        createTransitionSet(viewGroup, 195, new AccelerateInterpolator(), transitions);
    }

    public static void createTransitionEnteringScreenOff(@NonNull ViewGroup viewGroup, Transition... transitions) {
        createTransitionSet(viewGroup, 195, new DecelerateInterpolator(), transitions);
    }

    public static void createTransitionSet(@NonNull ViewGroup viewGroup, int duration, TimeInterpolator timeInterpolator, Transition... transitions) {
        TransitionSet set = new TransitionSet();
        set.setOrdering(0);
        Transition[] var5 = transitions;
        int var6 = transitions.length;

        for(int var7 = 0; var7 < var6; ++var7) {
            Transition transition = var5[var7];
            set.addTransition(transition);
        }

        set.setDuration(225L);
        set.setInterpolator(timeInterpolator);
        TransitionManager.beginDelayedTransition(viewGroup, set);
    }

    public static void createTransitionSet(@NonNull ViewGroup viewGroup, int duration, Transition... transitions) {
        TransitionSet set = new TransitionSet();
        set.setOrdering(0);
        Transition[] var4 = transitions;
        int var5 = transitions.length;

        for(int var6 = 0; var6 < var5; ++var6) {
            Transition transition = var4[var6];
            set.addTransition(transition);
        }

        set.setDuration((long)duration);
        set.setInterpolator(new AccelerateInterpolator());
        TransitionManager.beginDelayedTransition(viewGroup, set);
    }

    public static void beginDelayedTransition(@NonNull ViewGroup viewGroup, Transition transition) {
        TransitionManager.beginDelayedTransition(viewGroup, transition);
    }

    public static Transition createTransitionToLeft(View view, boolean isScreeOn) {
        return createTransitionTo(view, isScreeOn, 8388611);
    }

    public static Transition createTransitionToRight(View view, boolean isScreeOn) {
        return createTransitionTo(view, isScreeOn, 8388613);
    }

    public static Transition createTransitionToTop(View view, boolean isScreeOn) {
        return createTransitionTo(view, isScreeOn, 48);
    }

    public static Transition createTransitionToBottom(View view, boolean isScreeOn) {
        return createTransitionTo(view, isScreeOn, 80);
    }

    private static Transition createTransitionTo(View view, boolean isScreeOn, int gravity) {
        Slide slide = new Slide();
        slide.setSlideEdge(gravity);
        slide.addTarget(view);
        if (isScreeOn) {
            slide.setDuration(300L);
            slide.setInterpolator(new FastOutLinearInInterpolator());
        } else {
            slide.setInterpolator(new LinearInterpolator());
        }

        return slide;
    }
}
