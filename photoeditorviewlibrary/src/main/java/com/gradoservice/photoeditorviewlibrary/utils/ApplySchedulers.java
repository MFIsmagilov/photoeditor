package com.gradoservice.photoeditorviewlibrary.utils;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.ObservableTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


public class ApplySchedulers<D> implements ObservableTransformer<D, D> {

    @Override
    public ObservableSource<D> apply(Observable<D> upstream) {
        return upstream
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
