package com.gradoservice.photoeditorviewlibrary.utils;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.location.Location;
import androidx.exifinterface.media.ExifInterface;

import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by maratismagilov on 28.03.2018.
 */
public class BitmapUtils {
    public BitmapUtils() {
    }

    /**
     * Sets the text size for a Paint object so a given string of text will be a
     * given width.
     *
     * @param paint
     *            the Paint to set the text size for
     * @param desiredWidth
     *            the desired width
     * @param text
     *            the text that should be that width
     */
    private static void setTextSizeForWidth(Paint paint, float desiredWidth,
                                            String text) {

        // Pick a reasonably large value for the test. Larger values produce
        // more accurate results, but may cause problems with hardware
        // acceleration. But there are workarounds for that, too; refer to
        // http://stackoverflow.com/questions/6253528/font-size-too-large-to-fit-in-cache
        final float testTextSize = 64f;

        // Get the bounds of the text, using our testTextSize.
        paint.setTextSize(testTextSize);
        Rect bounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), bounds);

        // Calculate the desired size as a proportion of our testTextSize.
        float desiredTextSize = testTextSize * desiredWidth / bounds.width();

        // Set the paint for that size.
        paint.setTextSize(desiredTextSize);
    }

    public static void setDateToBitmap(Bitmap bitmap, Date date) throws IOException {
        if (date != null) {
            int width = bitmap.getWidth();
            int height = bitmap.getHeight();
            Canvas canvas = new Canvas(bitmap);
            Paint paint = new Paint();
            paint.setColor(Color.parseColor("#FF7700"));
//            int textSize = 150 * (width > height ? width : height) / 2800;
//            paint.setTextSize((float) textSize);
            paint.setTextAlign(Paint.Align.RIGHT);
            long snapshotTime = date.getTime();
            setTextSizeForWidth(paint, width - 120, getSimpleDateFormat().format(new Date(snapshotTime)));
            float textSize = paint.getTextSize();
            canvas.drawText(getSimpleDateFormat().format(new Date(snapshotTime)), (float) (canvas.getWidth() - textSize), (float) (canvas.getHeight() - textSize), paint);
        }

    }

    @Deprecated
    public static void setExifToPhoto(Location location, String capturedFilePath) throws IOException {
        ExifInterface exifInterface = new ExifInterface(capturedFilePath);
        exifInterface.setGpsInfo(location);
        exifInterface.saveAttributes();
    }

    @Deprecated
    public static void setExifToPhoto(String text, String capturedFilePath) throws IOException {
        //todo: кажется это не работает
        ExifInterface exifInterface = new ExifInterface(capturedFilePath);
        exifInterface.setAttribute(ExifInterface.TAG_USER_COMMENT, text);
        exifInterface.saveAttributes();
    }

    public static void saveBitmap(Bitmap bitmap, int quality, String capturedFilePath) throws IOException {
        FileOutputStream out = new FileOutputStream(capturedFilePath);
        bitmap.compress(Bitmap.CompressFormat.JPEG, quality, out);
        out.close();
    }

    public static SimpleDateFormat getSimpleDateFormat() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm z");
        return sdf;
    }
}
