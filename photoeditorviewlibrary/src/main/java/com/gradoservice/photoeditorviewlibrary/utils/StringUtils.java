package com.gradoservice.photoeditorviewlibrary.utils;

/**
 * Created by maratismagilov on 23.03.2018.
 */

public class StringUtils {
    public StringUtils() {
    }

    public static boolean stringIsNotEmpty(String string) {
        return string != null && !string.equals("null") && !string.trim().equals("");
    }
}
